# Pearson-Potekhin analytical representation for one- and two-parameter equations of state

- [Description](#description)
- [Credit](#credit)
- [Download and installation](#download-and-installation)
- [Project structure and usage](#project-structure-and-usage)
  - [One-parameter EoSs](#one-parameter-eoss)
  - [Two-parameter EoSs](#two-parameter-eoss)
- [Testing](#testing)

## Description
This project contains a series of `python` scripts that performs an analytical fit to one- or two-parameter equations of state (EoS, i.e. pressure as a function of mass-energy density, or pressure as a function of mass-energy density and electronic charge fraction) from the `CompOSE` database. We use the analytical respresentation described by [Haensel & Potekhin 2004, A&A, 428, 191-197](https:doi.org/10.1051/0004-6361:20041722) and [Pearson et al. 2018, MNRAS, 481, 2994-3026](https://doi.org/10.1093/mnras/sty2413) to perform a fit to the high-density core region, while a polytropic model is added for the crust.  We use the Generalised Piecewise Polytrope scheme described in [Boyle et al. 2020, Phys. Rev. D, 102, 083027](https://doi.org/10.1103/PhysRevD.102.083027) to ensure continuity in pressure, sound speed and energy-density across the crust-core boundary.

The analytical fits are then used to calculate thermodynamic quantities needed for hydrodynamical simulations of oscillating neutron stars (see [Servignat et al. 2023a](https://doi.org/10.1088/1361-6382/acc828)). The corresponding hydrodynamical code is here: https://gitlab.obspm.fr/novak/NS-hydro, and is based on the [Lorene](https://lorene.obspm.fr/) numerical relativity library.

The code was used for the study presented in [Servignat et al. 2023b](https://doi.org/10.1103/PhysRevD.109.103022), which also contains further details on the fitting procedure. 

In this study, the following one-parameter EoSs were considered:
* [APR](https://compose.obspm.fr/eos/68),
* [SLy4](https://compose.obspm.fr/eos/134)
* [(GPPVA)DD2](https://compose.obspm.fr/eos/217),

and for the two-parameter EoSs:
* [(SRO)APR](https://compose.obspm.fr/eos/149),
* [(RG)SLy4](https://compose.obspm.fr/eos/148),
* [(HS)DD2](https://compose.obspm.fr/eos/18).

The `CompOSE` database, as well as the manual, can be found via: https://compose.obspm.fr/

## Credit
This code was developed in collaboration with members of the LuTH-Caen group within the Virgo collaboration, and within the framework of the Agence Nationale de la Recherche (ANR) under contract ANR-22-CE31-0001-01 and from the CNRS International Research Project (IRP)
“Origine des éléments lourds dans l’univers:
Astres Compacts et Nucléosynthèse (ACNu)”.

## Download and installation

To clone the code via HTTPS or SSH, run the corresponding commands on your machine.

From the IN2P3 GitLab instance (for developers):
```
git clone https://gitlab.in2p3.fr/eos-for-virgo/eos-fits.git
```
or
```
git clone https://gitlab.in2p3.fr/eos-for-virgo/eos-fits.git
```
via `HTTPS`.

From the publicly accessible `CompOSE` repository:

```
git clone git@gitlab.obspm.fr:data_and_software_compose/fit_pearson-potekhin.git
```
via `ssh` or
```
git clone https://gitlab.obspm.fr/data_and_software_compose/fit_pearson-potekhin.git
```
via `HTTP`.

Next, navigate to the project directory:
```
cd eos-fits
```
or
```
cd fit_pearson-potekhin
```
and install the software environment via `Conda` (you will need to make sure that `Conda` is installed on your machine):
```
conda env create -f environment.yml
```

Once the environment has been created, you can activate or desctivate it by running, respectively,
```
conda activate eos-fit-env
```
or
```
conda deactivate
```

## Project structure and usage

### Project structure
The broad structure of the project is given below:

* `code`: contains the software to generate the `CompOSE` tables for a given EoS, and then to perform the fits:
  - `code-compose`: a `git` sub-module that points to the [`CompOSE` code repository](https://gitlab.obspm.fr/data_and_software_compose/code-compose)
  - `configs`: Contain configuration files, for each of the EoS considered in the above study, that define the fitting parameters,
  - `fitter`: Contains Python modules used by the fitting procedure,
  - `tests`: contains code unit tests.
* `environment.yml`: `YAML` file that specifies the required libraries and their versions,
* `inputs`: `CompOSE` tables are created and stored under `inputs/eos-tables/<eos_name>` where `eos_name` is the `name of the EoS. For a given EoS, there are two sub-directories:
  - `inputs/eos-tables/<eos_name>/core`: This contains an `eos.parameters` and `eos.quantities` file, needed by the `CompOSE` software, to create the tables for the high-density part of the NS. The fitting is performed on this table,
  - `inputs/eos-tables/<eos_name>/ref`: This is similar, except creates a "reference" table which is used to compare the results of the fitting scheme.
* `outputs`: For a given EoS, files that are created by the fitting code are output to `outputs/<eos_name>`. Figures showing the performance of the fits are written to `outputs/<eos_name>/figs`.
* `Tools`: contains miscellaneous scripts that are not dierctly needed for the fitting procedure.

### Usage
Navigate to the code directory:
```
cd code
```

#### One-parameter EoSs
For a one-parameter EoS, e.g. APR, the fitting procedure can be run by running
```
python Scr_RunEoSFits.py configs/APR.conf
```
The key output, located in `output/APR/` is `eos_fit_APR.d`, which is a Lorene-formatted table containing baryon number density (`n_B [fm^-3]`), mass-energy density (`e [g/cm^3]`) and the pressure (`P [dyn/cm^2]`). This is needed for the hydrodynamocical code to create the NS structure.

Other auxillary outputs are:
* `crust_parameters_APR.dat`: contain the values of $`\gamma`$ and $`\kappa`$ for the polytropic crust, the values of $`\kappa_{GPP}`$, $`\Gamma_{GPP}`$, $`L_{GPP}`$ and $`d_{GPP}`$ for the "intermediate" zone linking the the crust to the core, and $`L`$ and $d$ for the high-density core region,
* `fit_parameters_APR_Yq_0.0000.dat`: Contains the Pearson-Potekhin fitting coefficients once the minimization procedure, that adds the crust to the core, has converged,
* `init_fit_arameters_APR_Yq_0.0000.dat`: Similar to the above, but after an initial fit to the core has been performed, and before the minimization procedure has started,
* `eos.thermo`: Contains thermodynamic properties, generated by the fit, in the `CompOSE` format. See Sec. 4.2.2 of the [`CompOSE` manual](https://compose.obspm.fr/manual) for details. Only needed for diagnostic purposes, 

#### Two-parameter EoSs

##### Fitting

As for the one-parameter case, the fitting procedure can be started via (e.g. for the the case of (RG(SLy4))):
```
python Scr_RunEoSFits.py configs/rgsly4.conf
```
and generates the following output:

* `eos_fit_RGSLy4.d`: this is similar to the one-parameter EoS table but contains additional variables on a baryon number density-electronic charge fraction grid:
  - Baryon number density: `n_B [1/fm^{3}]`,
  - mass-energy density: `e [g/cm^3]`
  - pressure: `P [dyn/cm^2]`,
  - log of the enthalpy: `H [c^2]`,
  - Electron charge fraction `Y_e1 [adim]`,
  - Square of the sound speed: `c_s^2 [c^2]`,
  - Electron chemical potential: `mu_e [MeV]`,
  - Second derivative of pressure w.r.t log enthalpy and the electron charge fraction: `d2p/dHdYe [dyn c^-2 cm^-2]`,
  - Neutrino production rate: `Prod rate [s^-1]`. Always zero for cold NS matter.
* `eos.[nb,yq,t,thermo]`: EoS tables in the `CompOSE` format containing, respectively, the grid values for the baryon number density, charge fraction, temperature and thermo dynamic variables (see [`CompOSE` manual](https://compose.obspm.fr/manual)) for details. These are needed to create the beta-equilibrated tables, as described in [Beta-equilibrium tables](#beta-equilibrium-tables),
* `crust_parameters_RGSLy4.dat`: Crust parameters,
* `[init_]fit_parameters_RGSLy4_Yq_<yq>.dat`: fit coefficients for each slice of the charge fraction.

In contrast to the one-parameter cases, we need to run two additional steps.

##### Beta-equilibrium tables
In order to calculate the equilibrium structure of the NS for the hydrodyanamical simulations we need to create the beta-equilibrated version of the two-parameter tables. Using (RG)SLy4 as an example, this is done by running
```
python Scr_MakeBetaEquilFiles.py ../outputs/RGSLy4 configs/rgsly4.conf
```
and produces the following output:
* `eos_fit_beta_RGSLy4.d`: Contains baryon number density, mass-energy density and pressure as described in [One-parameter EoSs](#one-parameter-eoss),
* `ye_beta_RGSLy4.d`: Contains the log of the enthalpy and the electron charge fraction.

##### Orthogonalisation
Finally, the file `eos_fit_RGSLy4.d` produced by the step described in [Fitting](#fitting) contains thermodynamic variables on an orthogonal $`(n_{B}, Y_{e})`$ grid. However, the mathematical formalism used by the hydrodynamical code is based on a log-enthalpy-charge fraction grid, i.e. $`(H, Y_{e})`$. We therefore need to re-orthogonalise the table onto the new $`(H, Y_{e})`$ grid by running:
```
python Scr_OrthogonaliseTable.py ../outputs/RGSLy4 config/rgsly4.conf
```
The re-orthogonalised table is located in: `outputs/RGSLy4/eos_fit_RGSLy4_orthog.d`.

In summary, for the hydrodynamical simulations, we need to provide the following:
* `eos_fit_RGSLy4_orthog.d`,
* `eos_fit_beta_RGSLy4.d`,
* `ye_beta_RGSLy4.d`.

## Testing
The code comes with several unit tests, which can be launched, from within the `code` directory, with the following command:
```
./run_tests.sh
```
All tests should pass, as indicated via the $`\textcolor{green}{\textsf{PASS}}`$
