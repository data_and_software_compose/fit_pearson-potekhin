"""
Module containing helper functions for the unit tests.

"""
import numpy as np

from fitter.consts import MEV_TO_ERG, C_CGS, FM_TO_CM


def read_files(kgo_path, test_path, nskip, usecols=None):
    """
    Read in an output file generated from the fitting procedure,
    calculated from the Known Good Output, and from the test run.

    :arg: str kgo_path  : Path to the KGO data
    :arg: str test_path : Path to the test data
    :arg: int nskip     : Num. of rows to skip
    :arg: tuple usecols : Specify columns to read in.

    """
    kgo_data = np.loadtxt(kgo_path, skiprows=nskip, usecols=usecols)
    test_data = np.loadtxt(test_path, skiprows=nskip, usecols=usecols)

    if kgo_data.ndim == 1:
        # Reshape any 1-D arrays into 2-D arrays, so that we
        # can handle data comparisons more easily.
        kgo_data = kgo_data.reshape((kgo_data.size, 1))
        test_data = test_data.reshape((test_data.size, 1))

    return kgo_data, test_data


def data_compares(kgo_path, test_path, nskip, usecols=None, tol=1.e-4):
    """
    Check whether data contained in KGO and test output files
    are below a given tolerance.

    :arg: str kgo_path  : Path to the KGO data
    :arg: str test_path : Path to the test data
    :arg: int nskip     : Num. of rows to skip
    :arg: tuple usecols : Specify columns to read in
    :arg: float tol     : Tolerance in relative error between test
                          and KGO data

    """
    data_compares = True

    kgo_data, test_data = read_files(kgo_path, test_path, nskip, usecols)

    # Use any column to check that the number of rows are equal.
    assert test_data.shape[0] == kgo_data.shape[0], \
        "The number of rows between test and KGO data are not equal."

    ncols = kgo_data.shape[1]
    for ncol in range(0, ncols):
        kgodata = kgo_data[:, ncol]
        testdata = test_data[:, ncol]

        if not is_below_tolerance(kgodata, testdata, tol):
            data_compares = False
            break

    return data_compares


def press_poly(kappa, gamma, nB):
    """
    Return the pressure for a polytrope according to the expression
    kappa x nB**gamma where nB is the baryon number density.

    """
    return kappa * nB**gamma


def energy_density_poly(press, gamma, mn, nB):
    """
    Return the energy density for a polytrope according to the
    expression P / (gamma - 1) + (mn x nB) where mN is the particle
    mass

    """
    return (press / (gamma - 1.e0)) + mn*nB


def mass_energy_density(energy_density):
    """
    Return the mass energy density (g/cm^3) from the energy density
    (MeV/fm^3).

    """
    return energy_density * (MEV_TO_ERG / C_CGS**2) * (1.e0/FM_TO_CM)**3


def pressure_toy_crust(nB, Yq):

    """
    Simple analytical expression to calculate the pressure in order to
    test calculation of other thermodynamic quantities. Depends on
    baryon num. density, nB, and charge fraction, Yq.

    """
    return nB**2 + Yq


def energy_density_toy_crust(nB, Yq, f0, n0):
    """
    Simple analytical expression to calculate the free energy density
    to test calculation of other thermodynamic quantities. Expression
    determined from above expression for pressure, via

    p = nB^2 x d(f/n)/dn

    :arg: float nB : Baryon num density
    :arg: float Yq : Charge fraction
    :arg: float f0 : Initial value for energy density
    :arg: float n0 : First grid value for the baryon num, density.

    """
    # Calculate the integration constant
    k = (f0 + Yq - n0**2) / n0
    return nB * (nB + k) - Yq


def scaled_free_energy(f, nB, Mn):
    """
    Calculate the scaled free energy density

    :arg: float f  : Free energy density
    :arg: float nB : Baryon number density
    :arg: float Mn : Neutron mass

    """
    return (f / (Mn*nB)) - 1.e0


def square_sound_speed_toy_crust(nB, Yq, f0, n0):
    """
    Calculate the square of the sound speed, based
    on the above simple analytical expressions for the
    pressure and the free energy density.

    """
    k = (f0 + Yq - n0**2) / n0
    return 2.e0 * nB / (2.e0*nB + k)


def chemical_potential_toy_crust(nB, n0):
    """
    Calculate the lepton chemical potential based
    on the above simple analytical expression for
    the free energy density.

    """
    return (1.e0 / n0) - (1.e0 / nB)


def log_enthalpy_toy_crust(nB, Mn, Yq, f0, n0):
    """
    Calculate the log enthalpy based on the simple analytical
    expressions for the pressure and free energy density.

    """
    k = (f0 + Yq - n0**2) / n0
    return np.log((2.e0*nB + k) / Mn)


def d2p_dlogHdYq_toy_crust(nB, n0):
    """
    Second derivative of the pressure w.r.t. log enthalpy
    and the charge fraction.

    """
    return nB / n0


def is_below_tolerance(refval, testval, tol):
    """
    Check whether the relative error between a reference
    and test value is below a specified tolerance.

    """
    below_tolerance = True

    if type(refval) != np.ndarray:
        refval = [refval]
        testval = [testval]

    for tval, rval in zip(testval, refval):
        # To prevent a divide by zero, skip if the KGO and
        # test data is zero.
        if (tval == 0.) and (rval == 0.):
            continue
        else:
            err = np.abs(tval - rval) / rval

            if err > tol:
                print(f'Not below tolerance for {tval} and {rval}.')
                below_tolerance = False
                break

    return below_tolerance
