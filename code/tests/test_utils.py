"""Unit tests for the utils.py module"""
import configparser
import copy
import os
import shutil
import tempfile
import unittest

import numpy as np

from pathlib import Path
from unittest.mock import patch, call
from unittest import mock

import fitter.utils

TMPDIR = tempfile.mkdtemp()


class TestReadConfig(unittest.TestCase):
    """Unit tests for the utils.read_config_file() function."""
    def setUp(self):
        # Create some test configuration files with complete sections,
        # with a missing section, and with a missing section entry,
        # respectively.
        self.configfile = os.path.join(TMPDIR, 'test.conf')
        self.configfile_miss_sect = os.path.join(TMPDIR, 'miss_sect.conf')
        self.configfile_miss_entry = os.path.join(TMPDIR, 'miss_entry.conf')

        self.config = configparser.ConfigParser()
        self.config['eos'] = {
            'url': 'https://path/to/compose/table',
            'table_path': '/path/to/eos/table',
            'name': 'RGSLy4',
            'compose_code': '/path/to/compose/software'
            }

        self.config['fitting'] = {
            'run_fit': True,
            'tolerance': 1.e-3,
            'method': 'trf',
            'mintol': 1.e-3,
            'minmethod': 'trust-constr',
            'nBlim1': 1.e-3,
            'nBlim2': 0.05,
            'kappa': 'sroapr',
            'gamma': 1.7
            }

        self.config['nb_model_grid'] = {
            'nsize': 400,
            'start': 1.e-6,
            'end': 1.45e0
            }

        self.config['plots'] = {
            'make_plots': True,
            'figdir': '/figs',
            'yq_list': [0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
            'ref_table': '/path/to/eos.table',
            'ref_lorene': '/path/to/ref/lorene/file',
            'lorene_file': 'path/to/lorene/file',
            'plot_errors': True
            }

        with open(self.configfile, 'w') as handle:
            self.config.write(handle)

    @staticmethod
    def remove_test_data():
        shutil.rmtree(TMPDIR)

    @classmethod
    def tearDownClass(self):
        self.remove_test_data()

    def test_read_config(self):
        """Check that the config file is read correctly."""
        data = fitter.utils.read_config_file(self.configfile)

        self.assertEqual(data, self.config)

    def test_no_file_found(self):
        """Check that an error is thrown if the file does not exist."""
        config = '/no/file/here'
        expected_err = f'File {config} does not exist.'

        with self.assertRaises(SystemExit) as excep:
            fitter.utils.read_config_file(config)

        self.assertEqual(str(excep.exception), expected_err)

    def test_missing_section(self):
        """Check that an error is raised if a section is missing."""
        expected_err = (
            'Required section "eos" not found in '
            f'{self.configfile_miss_sect}.'
        )

        bad_config = copy.copy(self.config)
        bad_config.remove_section('eos')

        with open(self.configfile_miss_sect, 'w') as handle:
            bad_config.write(handle)

        with self.assertRaises(SystemExit) as excep:
            fitter.utils.read_config_file(self.configfile_miss_sect)

        self.assertEqual(str(excep.exception), expected_err)

    def test_missing_entry(self):
        """
        Check that an error is raised if an entry under a given section is
        missing.

        """
        expected_err = 'Required entry "name" not in "eos".'
        bad_config = copy.copy(self.config)
        bad_config.remove_option('eos', 'name')

        with open(self.configfile_miss_entry, 'w') as handle:
            bad_config.write(handle)

        with self.assertRaises(SystemExit) as excep:
            fitter.utils.read_config_file(self.configfile_miss_entry)

        self.assertEqual(str(excep.exception), expected_err)


class TestCheckExistingOutput(unittest.TestCase):
    """Unit tests for the check_existing_output function."""

    def setUp(self):
        if not os.path.exists(TMPDIR):
            os.mkdir(TMPDIR)

        self.eos_name = 'RGSLy4'
        self.nb_grid_path = os.path.join(TMPDIR, 'eos.nb')
        self.lorene_path = os.path.join(TMPDIR, f'eos_fit_{self.eos_name}.d')

        self.nb_grid_file = Path(self.nb_grid_path).touch()
        self.lorene_file = Path(self.lorene_path).touch()

    @staticmethod
    def remove_test_data():
        shutil.rmtree(TMPDIR)

    @classmethod
    def tearDownClass(self):
        self.remove_test_data()

    @patch("fitter.utils.log.info")
    def test_no_deletion(self, info_mock):
        """
        Check we exit the code if the user chooses not to delete
        output files.

        """
        mock.builtins.input = lambda _: 'n'

        with self.assertRaises(SystemExit):
            fitter.utils.check_existing_output(self.eos_name, TMPDIR)

        info_mock.assert_called_once_with(
            "Keeping files. Aborting fit procedure.")

    @patch("fitter.utils.log.info")
    def test_delete_output(self, info_mock):
        """
        Check that the function attempts to delete the output
        files if the user chooses the delete option.

        """
        mock.builtins.input = lambda _: 'y'

        fitter.utils.check_existing_output(self.eos_name, TMPDIR)

        messages = [
            call(f'Deleting {self.nb_grid_path}.'),
            call(f'Deleting {self.lorene_path}.')
            ]

        info_mock.assert_has_calls(messages)


class TestBisection(unittest.TestCase):
    """Unit tests for the fitter.utils.bisection function"""

    @staticmethod
    def quadfunc(x, a, b, c):
        """Define a quadratic function on which to test the bisection
        routine.

        """
        return a*x*x + b*x + c

    def setUp(self):
        self.tol = 1.e-6
        self.x = np.linspace(0, 10, 100)
        self.expected_root = 2.0
        self.a = 1.0
        self.b = 0.
        self.c = -4.

    def test_bisection(self):
        """Check that root can be found for a quadratic equation."""
        root = fitter.utils.bisection(
            self.x, self.quadfunc, self.a, self.b, self.c)

        self.assertTrue(
            self.expected_root - root <= self.tol)

    def test_bisection_adjust_brackets(self):
        """Check that root can be found for a quadratic equation
        where the bracketing values are automatically found."""
        # Provide a fixed value of the independent variable, from
        # which the bracketing values are automatically found.
        root = fitter.utils.bisection(
            1.0, self.quadfunc, self.a, self.b, self.c)

        self.assertTrue(
            self.expected_root - root <= self.tol)

    def test_bisection_failure(self):
        """Check that an error is thrown if the root cannot be found."""

        with self.assertRaises(SystemExit) as excep:
            # Change the sign of the 'c' coefficient so that a root
            # cannot be found.
            fitter.utils.bisection(
                self.x, self.quadfunc, self.a, self.b, -self.c)

            self.assertEqual(
                excep.exception,
                'Bracketing values could not be found.'
            )


if __name__ == '__main__':
    unittest.main()
