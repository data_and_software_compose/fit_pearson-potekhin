"""Tests for the Scr_MakeBetaEquilFiles.py"""
import os
import shutil
import subprocess
import tempfile
import unittest

import fitter.consts

from helpers import data_compares

KGI = 'tests/data/kgi/RGSLy4/beta-equil'
KGO = 'tests/data/kgo/RGSLy4/'

TMPDIR = tempfile.mkdtemp()


class TestMakeBetaEquilFiles(unittest.TestCase):
    """
    Check that the hydro input files from the beta-equilibrated
    tables are generated correctly.

    """
    def setUp(self):
        self.eos_name = 'RGSLy4'
        self.eos_beta = f'eos_fit_beta_{self.eos_name}.d'
        self.ye_beta = f'ye_beta_{self.eos_name}.d'
        self.script = 'Scr_MakeBetaEquilFiles.py'

        # Copy the EoS tables from the fitting procedure into
        # the temporary directory
        self.tables = [
            'eos.t', 'eos.thermo', 'eos.yq', 'eos.nb'
        ]

        for table in self.tables:
            src = os.path.join(KGO, table)
            dest = os.path.join(TMPDIR, table)
            shutil.copy(src, dest)

    def tearDown(self):
        shutil.rmtree(TMPDIR)

    def test_make_beta_files(self):
        """
        Check that the eos_fit_beta_*.d and ye_beta_*.d files are
        created correctly.

        """
        cmd = './{script} {outdir} {config}'.format(
            script=self.script,
            outdir=TMPDIR,
            config='configs/rgsly4.conf')

        output = subprocess.run(
            cmd, shell=True, capture_output=True,
            text=True
        )

        kgo_path = KGO
        test_path = os.path.join(
             f'../{fitter.consts.OUTPUTDIR}', self.eos_name)

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.eos_beta),
                os.path.join(test_path, self.eos_beta),
                nskip=9
            ), f'File {self.eos_beta} does NOT compare.'
        )

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.ye_beta),
                os.path.join(test_path, self.ye_beta),
                nskip=0
            ), f'File {self.ye_beta} does NOT compare.'
        )


if __name__ == '__main__':
    unittest.main()
