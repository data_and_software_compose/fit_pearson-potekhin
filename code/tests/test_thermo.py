"""Unit tests for the thermo.py module."""
import unittest
import unittest.mock

import numpy as np

from fitter.thermo import (
    calc_baryon_num_density, dlognb_drho, has_nans, has_infs,
    _calc_rho, dPdn_pearson, dPdn_polytrope,
    energy_density_polytrope
)
from fitter.consts import MEV_FMCUBED_TO_ERG_CMCUBED, C_CGS, FM_TO_CM

from helpers import is_below_tolerance


class TestHasNans(unittest.TestCase):
    """Unit tests for the has_nans() function."""

    def test_has_nans(self):
        """Check True is returned for an array with NaNs."""
        array = np.array([1, 2, 3, np.nan, 4])
        self.assertTrue(has_nans(array))

    def test_no_nans(self):
        """Check False is returned for an array with no NaNs."""
        array = np.array([1, 2, 3, 4])
        self.assertFalse(has_nans(array))


class TestHasInfs(unittest.TestCase):
    """Unit tests for the has_infs() function."""

    def test_has_infs(self):
        """Check True is returned for an array with infinities."""
        array = np.array([1, 2, 3, np.inf])
        self.assertTrue(has_infs(array))

    def test_no_infs(self):
        """Check false is returned for an array with no infinities."""
        array = np.array([1, 2, 3, 4])
        self.assertFalse(has_infs(array))


class TestSolveBaryonDensity(unittest.TestCase):
    """
    Set up tests to verify the calculation of the baryon number
    density. Use a polytropic model so that we can check the results
    analytically. For simplicity, there is no rest mass energy (Mn=0)
    contribution to the energy density.

    """
    def setUp(self):
        self.nB = np.linspace(0.1e0, 1.e0, 50)
        self.kappa = 100.e0
        self.gamma = 2.e0
        self.tol = 1.e-4

        # Total energy density in erg/cm^3
        self.e_density = energy_density_polytrope(
            self.nB, self.kappa, self.gamma, mu0=0.) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        # Mass-energy density (g/cm^3)
        self.rho = self.e_density / C_CGS**2.e0
        self.rho0 = self.rho[0]
        self.rhoend = self.rho[-1]
        self.lognB0 = np.log(self.nB[0]/FM_TO_CM**3)
        self.press = 10.**self.log_press_poly(np.log10(self.rho), self.gamma)
        self.temp = 0.e0

        # Set up a mock object for an EoS table, along with a mock for
        # the search method to return the first grid point enthalpy
        # per baryon, and dummy grid values for nB and Yq.
        self.eos_table = unittest.mock.Mock()
        self.eos_table.search = unittest.mock.MagicMock(side_effect=self.h0)
        self.eos_table.Ygrid = np.array([0.0])
        self.eos_table.NBgrid = self.nB

    def test_dlognb_drho(self):
        """
        Check that the function dlognb_drho returns the correct value
        for the derivative of the log of the baryon density w.r.t
        the mass-energy density.

        """
        dlogn_drho_expected = 1.e0 / (self.gamma * self.rho)

        # The calling sequence to this function is needed by
        # the scipy solve_ivp solver, which requires that the second
        # argument is the dependent variable that we want to solve for.
        # For the sake of just testing dH_drho, this isn't needed.
        lognB = None

        # TO-DO the argument Yq = None should be removed as is
        # now a redundant argument.
        dlogn_drho = dlognb_drho(
            self.rho, lognB, None, [self.gamma], self.log_press_poly)

        self.assertTrue(
            is_below_tolerance(dlogn_drho_expected, dlogn_drho, self.tol)
            )

    def test_calc_baryon_num_density(self):
        """
        Check that the baryon number density calculated by integrating the
        derivative of the log of the baryon number density w.r.t the
        mass energy density is consistent with the values set in the
        setUp method.

        """
        nB = calc_baryon_num_density(
            self.rho0, self.rhoend, self.lognB0, [self.gamma],
            None, self.rho, self.log_press_poly)

        self.assertTrue(
            is_below_tolerance(self.nB, nB, self.tol)
            )

    def test_calc_rho(self):
        """
        Check that the correct value of the mass-energy density
        is returned for a given value of the baryon number
        density.

        """
        for rho, nB in zip(self.rho, self.nB):
            rho_expected = _calc_rho(
                self.eos_table, 0, self.temp, self.log_press_poly,
                [self.gamma], self.rho0, nB)

            self.assertTrue(
                is_below_tolerance(rho_expected, rho, self.tol)
            )

    def test_dPdn_fit(self):
        """
        Check that the derivative of the pressure w.r.t the baryon
        number density, calculated via a fit function relating the
        pressure and mass-energy density, gives expected values.

        """
        for nB in self.nB:
            dPdn_expected = dPdn_polytrope(
                nB, self.kappa, self.gamma) * \
                MEV_FMCUBED_TO_ERG_CMCUBED * FM_TO_CM**3

            dPdn = dPdn_pearson(
                nB, [self.gamma], self.eos_table, self.temp,
                self.log_press_poly, self.rho0,
                fit_deriv=self.dlog_press_dlog_rho_poly)

            self.assertTrue(
                is_below_tolerance(dPdn_expected, dPdn, self.tol)
            )

    @staticmethod
    def log_press_poly(logrho, *pars, cgs=True):
        """
        Calculate the pressure from the mass energy density using a
        polytropic model. The calling signature mimics the Pearson
        fitting scheme used, which calculates the log10 pressure from
        the log10 of the mass-energy density. pars[0] corresponds to
        gamma, and for simplicity Mn = 0.

        """
        return np.log10(pars[0] - 1.e0) + logrho + 2.*np.log10(C_CGS)

    @staticmethod
    def dlog_press_dlog_rho_poly(logrho, *pars, cgs=True):
        """
        Supply the derivative of the log10 pressure w.r.t
        the log10 mass-energy density which is 1 for the polytropic
        model considered here.

        """
        return 1.e0

    def h0(self, *args):
        """
        Return the enthalpy per baryon (erg) for the first grid point
        in baryon number density. Calling signature is needed to mock
        the calling of a method of eos.EoS class.

        """
        nB0 = self.nB[0] / FM_TO_CM**3
        return np.array(
            [(self.e_density[0] + self.press[0]) / nB0]
        )


if __name__ == '__main__':
    unittest.main()
