[eos]
# Path to the eos.table generated by ComPOSE
table_path = tests/data/kgi/RGSLy4/eos.table.core
# Path to the eos.thermo file for the EoS
thermo_path = tests/data/kgi/RGSLy4/
# Name of the EoS
name = RGSLy4

[fitting]
# An initial fit is performed along each Yq slice using the Pearson
# fit parameters for the first slice. A repeat of the fitting
# procedure is performed using the fit parameters at Yq = Ymax for the
# first slice.
Ymax = -9999
# Fit tolerance
tolerance = 1.e-4
# Fitting method (trf or lm)
method = trf
# Apply a linear smoothing of tabulated pressure values in the range
# smooth_logrho_min <= log(rho / g cm^-3) <= smooth_logrho_max. Set to
# Comment out to deactivate.
#smooth_logrho_min = 13.75e0
#smooth_logrho_max = 14.53e0
# Value of the Baryon number density at the core/ crust boundary
nBlim = 0.08e0
# Value of the density above which we should apply the Pearson fitting
rhomin = 1.e7
# Provide an input file specifying the guess fit parameters.
# Comment out to use the Pearson default values.
#guess_pars_file=indata/guess_params_RGSLy4.dat
# Specify the number of grid points in rho for the interpolation of the
# tabulated pressure on the new, regularised grid of rho.
nrho = 1200

[nb_model_grid]
# Num. of grid points for the output model grid for the baryon number
# density
nsize = 400
# Start baryon number density (fm^-3)
start = 1.e-12
# End baryon number density (fm^-3)
end = 1.50e0

[plots]
# Set to True to create plots
make_plots = False
# Directory where the plots should be written to
figdir = figs/
# Specify values of the charge fraction for which we want to
# plot various thermo variables as a function of mass-energy density,
# as created by the fitting scheme and the ComPOSE database.
yq_list = [0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
# Specify location of the eos.table that we want to compare the fits to.
# It should be on the same model grid for nb as the script output.
ref_table = /home/pdavis/Work/LPC-Caen/EoS/RGSLY4/eos.table.ref
# Specify location of the Lorene-formatted file for two-parameter
# equation of state that we can use to compare the Pearson fits to.
# Comment out if no such file is available.
ref_lorene = tests/data/kgo/RGSLy4/eos_fit_RGSLy4_Gael.d