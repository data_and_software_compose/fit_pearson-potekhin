## KGI for data APR

* Tables downloaded from `https://compose.obspm.fr/download//1D/NS/Classical/APR/`,
* `eos.table.core` created using the `CompOSE` software using `eos.parameters` and `eos.quantities` files located in `../../../../../inputs/eos-tables/APR/core`,
* Generated using the `fitter.compose.CompOSE` class.