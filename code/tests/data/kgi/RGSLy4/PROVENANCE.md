## KGI data for RGSLy4

* Tables downloaded from `https://compose.obspm.fr/download/3D/Raduta/Neutrinos/Extended_version`
* `eos.table.core` created using the `CompOSE` software using `eos.parameters` and `eos.quantities` files located in `../../../../../inputs/eos-tables/RGSLy4/core`,
* Generated using the `fitter.compose.CompOSE` class.