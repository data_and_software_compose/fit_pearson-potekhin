"""Unit tests for the eos.py module."""
import os
import shutil
import unittest

import numpy as np

import fitter.consts
import fitter.compose

from fitter.eos import EoSFit, iT, EoS
from fitter.utils import read_config_file
from helpers import is_below_tolerance, data_compares

KGO = 'tests/data/kgo/'
KGI = 'tests/data/kgi/'


class TestReadEoS(unittest.TestCase):
    """Unit tests for the EoS class that handles the ComPOSE tables."""

    def setUp(self):
        self.eos_table = os.path.join(KGI, 'RGSLy4', 'eos.table.core')
        self.eos_thermo = os.path.join(KGI, 'RGSLy4')
        self.rgsly4_table = EoS(self.eos_table, self.eos_thermo)
        self.tol = 1.e-6
        self.nbstart = 0.05e0
        self.nbend = 1.5e0
        self.nbsize = 500
        self.yqstart = 0.01e0
        self.yqend = 0.6e0
        self.nyq = 60

        # Expected grid values in baryon number density is
        # on a regular logarithmic grid
        self.expected_nb = np.linspace(
            np.log10(self.nbstart), np.log10(self.nbend), self.nbsize)
        self.expected_nb = np.power(10, self.expected_nb)

        self.expected_yq = np.linspace(
            self.yqstart, self.yqend, self.nyq)

    def test_read_table(self):
        # TO-DO: add tests to check the reading in of the table.
        # Use a small sub-set of the table to keep the data small?
        pass

    def test_read_particle_info(self):
        """Check that the correct particle masses are returned."""
        mass_neutron_expected = 939.56536865234375
        mass_proton_expected = 938.272034
        has_leptons = 1

        self.assertTrue(
            is_below_tolerance(
                mass_neutron_expected,
                self.rgsly4_table.particles['mn'].to_numpy()[0],
                self.tol)
            )

        self.assertTrue(
            is_below_tolerance(
                mass_proton_expected,
                self.rgsly4_table.particles['mp'].to_numpy()[0],
                self.tol)
            )

        self.assertEqual(
            has_leptons,
            self.rgsly4_table.particles['lp'].to_numpy()[0])

    def test_search_table_by_yq(self):
        """
        Check that the correct values are retrieved when we filter
        via the temperature and the charge fraction.

        """
        temp = 0.1
        yq = 0.01e0

        # Searching for the Baryon number density should just return
        # the grid values
        vals = self.rgsly4_table.search(fitter.consts.NB_FM3, temp, yq)

        self.assertTrue(
            np.allclose(vals, self.expected_nb))

    def test_search_table_by_yq_and_nb_first_entry(self):
        """
        Check that the correct value is returned if we filter by the
        charge fraction and the baryon number density. Check that the
        entry associated with the first row, seventh column in the
        table is returned.

        """
        temp = 0.1e0
        yq = 0.01e0
        nb = 0.05e0

        expected_free_energy = 945.67772576701179

        val = self.rgsly4_table.search(
            fitter.consts.FREE_ENERGY_MEV, temp, yq, nb)

        self.assertTrue(
            is_below_tolerance(
                val[0],
                expected_free_energy,
                self.tol)
            )

    def test_search_table_by_yq_and_nb_arbitrary_entry(self):
        """
        Check that correct value is returned if we filter by the
        charge fraction and the baryon number density. Use a value
        of the baryon number density that is not associated with the
        start nor end rows of the table.

        """
        temp = 0.1e0
        yq = 0.01e0
        nb = 7.0303694183456417E-002

        expected_free_energy = 947.48057719139626

        val = self.rgsly4_table.search(
            fitter.consts.FREE_ENERGY_MEV, temp, yq, nb)

        self.assertTrue(
            is_below_tolerance(
                val[0],
                expected_free_energy,
                self.tol)
            )

    def test_search_table_by_yq_and_nb_end_entry(self):
        """
        Check that correct value is returned if we filter by the
        charge fraction and the baryon number density. Check that
        the value associated with the last row is returned.
        """
        # TO DO: investigate why this test fails when we try
        # and pick out the entry from the last row.
        pass

    def test_search_thermo_table_by_temp_and_nb(self):
        """
        Check that the correct value in scaled pressure is returned if we
        filter the table according to the temperature and baryon
        number index.  Corresponds to the first 60 rows in the table.

        """
        pressure_expected = np.array([
            3.2731420000000000, 3.2702599999999999, 3.2673809999999999,
            3.2645040000000001, 3.2616309999999999, 3.2587609999999998,
            3.2558929999999999, 3.2530280000000000, 3.2501669999999998,
            3.2473079999999999, 3.2444519999999999, 3.2416000000000000,
            3.2387500000000000, 3.2359030000000000, 3.2330600000000000,
            3.2302190000000000, 3.2273820000000000, 3.2245469999999998,
            3.2217159999999998, 3.2188870000000001, 3.2160620000000000,
            3.2132399999999999, 3.2104210000000002, 3.2076050000000000,
            3.2047919999999999, 3.2019829999999998, 3.1991779999999999,
            3.1963759999999999, 3.1935799999999999, 3.1907899999999998,
            3.1880109999999999, 3.1852610000000001, 3.1826279999999998,
            3.1806580000000002, 3.1798359999999999, 3.1791570000000000,
            3.1792410000000002, 3.1793230000000001, 3.1794099999999998,
            3.1795200000000001, 3.1797309999999999, 3.1800250000000001,
            3.1802250000000001, 3.1804239999999999, 3.1806489999999998,
            3.1808410000000000, 3.1810280000000000, 3.1811970000000001,
            3.1812999999999998, 3.1814320000000000, 3.1831920000000000,
            3.1846980000000000, 3.1862240000000002, 3.1878690000000001,
            3.1896599999999999, 3.1915840000000002, 3.1936070000000001,
            3.1956980000000001, 3.1978330000000001, 3.1999979999999999])

        vals = self.rgsly4_table.search_thermo(
            fitter.consts.PRESS_NB_MEV, 1, 1)

        self.assertTrue(
            np.allclose(vals, pressure_expected)
            )

    def test_search_thermo_table_by_temp_nb_yq(self):
        """
        Check that the correct value of scaled pressure is returned if we
        filter the table using the temperature, baryon number and
        charge fraction indices. Use the first index in each
        case. Should correspond to the first row, fourth column.

        """
        expected_pressure = [3.273142]

        vals = self.rgsly4_table.search_thermo(
            fitter.consts.PRESS_NB_MEV, 1, 1, 1)

        self.assertTrue(
            np.allclose(vals, expected_pressure)
            )

    def test_search_thermo_table_by_temp_nb_yq_arbitrary(self):
        """
        Check that the correct value of scaled pressure is returned if we
        filter the table using the temperature, baryon number and
        charge fraction indices. Use an arbitrary index in each case

        """
        expected_pressure = [2.4652272718347708]

        vals = self.rgsly4_table.search_thermo(
            fitter.consts.PRESS_NB_MEV, 22, 87, 59)

        self.assertTrue(
            np.allclose(vals, expected_pressure)
            )

    def test_Tgrid(self):
        """
        Check that the correct value for the grid in temperature
        is returned

        """
        T_expected = np.array([0.1e0])

        self.assertTrue(
            np.array_equal(T_expected, self.rgsly4_table.Tgrid)
            )

    def test_Ygrid(self):
        """
        Check that the correct values for the grid in charge fraction
        is returned.

        """
        self.assertTrue(
            np.allclose(self.expected_yq, self.rgsly4_table.Ygrid)
            )

    def test_NBgrid(self):
        """
        Check that the correct values for the grid in charge fraction
        is returned.

        """
        self.assertTrue(
            np.allclose(self.expected_nb, self.rgsly4_table.NBgrid)
            )


class TestOneParFitting(unittest.TestCase):
    """Unit tests for fits to a one-parameter equation of state."""

    @classmethod
    def setUpClass(self):
        self.apr_conf_file = os.path.join('configs/APR.conf')
        self.config = read_config_file(self.apr_conf_file)
        self.eos_name = 'APR'

        self.table_name = 'eos.table.core'
        self.thermo_file = 'eos.thermo'
        self.lorene_table = 'eos_fit_APR.d'
        self.fitpars = 'fit_parameters_APR_Yq_0.0000.dat'
        self.init_fitpars = 'init_fit_parameters_APR_Yq_0.0000.dat'
        self.crustpars = 'crust_parameters_APR.dat'

        self.table_path = self.config.get('eos', 'table_path')
        self.tables = fitter.compose.CompOSE(
            self.config.get('eos', 'url'),
            self.table_path,
            self.config.get('eos', 'compose_code')
        )

        self.tables.download_data()
        self.tables.make_tables(
            os.path.join(self.table_path, 'core', 'eos.quantities'),
            os.path.join(self.table_path, 'core', 'eos.parameters'),
            self.table_name)

        self.apr_fitter = EoSFit(self.config, self.table_name)

    @classmethod
    def tearDownClass(self):
        self.tables.housekeep()
        shutil.rmtree(f'../{fitter.consts.OUTPUTDIR}')

    def test_one_par_fitter(self):
        """
        Check that the fits to a one-parameter equation of state
        produces output data that matches Known Good Output.

        """
        kgo_path = os.path.join(KGO, self.eos_name)
        test_path = os.path.join(
            f'../{fitter.consts.OUTPUTDIR}', self.eos_name)

        self.apr_fitter.fit(iT, write_diags=True)

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.thermo_file),
                os.path.join(test_path, self.thermo_file),
                nskip=1
            ), f'File {self.thermo_file} does NOT compare.'
        )

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.lorene_table),
                os.path.join(test_path, self.lorene_table),
                nskip=9
            ), f'File {self.lorene_table} does NOT compare.'
        )

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.fitpars),
                os.path.join(test_path, self.fitpars),
                nskip=3, usecols=(2,)
            ), f'File {self.fitpars} does NOT compare.'
        )

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.init_fitpars),
                os.path.join(test_path, self.init_fitpars),
                nskip=3, usecols=(2,)
            ), f'File {self.init_fitpars} does NOT compare.'
        )

        self.assertTrue(
            data_compares(
                os.path.join(kgo_path, self.crustpars),
                os.path.join(test_path, self.crustpars),
                nskip=1,
            ), f'File {self.crustpars} does NOT compare.'
        )


class TestTwoParFitting(unittest.TestCase):
    """Unit tests for fits to a two-parameter equation of state."""
    # TO-DO: The fitting for two-par equations of state is too slow
    # to run in a unit test, and we should find a way to do this
    # using a limited data set?
    pass


if __name__ == '__main__':
    unittest.main()
