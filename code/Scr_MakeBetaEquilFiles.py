#!/usr/bin/env python3
"""
A script that creates data files for the NS hydro code. These files are
created via the ComPOSE code using the output generated from Scr_RunEoSFits.py
for beta-equilibrium.

"""
import argparse
import logging as log
import os

import numpy as np

import fitter.eos
import fitter.consts
import fitter.utils
import fitter.compose

LORENE_FIT_BETA = 'eos_fit_beta_{eos_name}.d'
LORENE_YE_BETA = 'ye_beta_{eos_name}{commit}.d'

TEST_MODE = os.getenv('TEST_MODE')

log.basicConfig(format='[%(levelname)s] %(module)s: %(asctime)s - %(message)s',
                level=log.INFO)


def write_beta_fit(nB, density, pressure, eos_name):
    """
    Write the Lorene EoS file for beta-equilibrated matter

    """
    fmt = '%6i', '%24.16E', '%24.16E', '%24.16E'

    columns = (
        '{0}{1:>30}{2:>28}'.format(
            'n_B [1/fm^3]', 'e [g/cm^3]', 'P [dyn/cm^3]')
        )

    # The branch and versions will change, so set these to an
    # empty string if we are running automated testing. This will
    # avoid test failures when comparing to KGO.
    git_version = (
        '\n' if TEST_MODE is not None
        else fitter.utils.git_latest_version()
    )
    git_branch = (
        '\n' if TEST_MODE is not None
        else fitter.utils.git_branch_name()
    )

    nbsize = len(nB)
    header = (
            f'# {eos_name} \n'
            '#\n'
            '# EoS created from fit of 2 parameter EoS for beta-equilibrated '
            'matter. Parameters are n_B\n'
            f'# {git_branch}'
            f'# {git_version}'
            f'{nbsize} in n_B\n'
            '#\n'
            f'#       {columns}\n'
            '#\n'
            )

    data = np.column_stack([range(nbsize), nB, density, pressure])

    outpath = f'../{fitter.consts.OUTPUTDIR}/{eos_name}'
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    fpath = os.path.join(
        f'{outpath}',
        LORENE_FIT_BETA.format(eos_name=eos_name)
    )

    with open(fpath, 'w') as handle:
        handle.write(header)
        np.savetxt(handle, data, fmt)


def write_ye_beta(logH, Ye, eos_name):
    """
    Write the ye_beta.d file containing the charge fraction
    for each value of the log enthalpy.

    """
    fmt = '%24.16E', '%24.16E'
    data = np.column_stack([logH, Ye])

    # If we are not running in test mode, add the commit version to
    # the name of the file containing the charge fraction profile.
    # Only consider the first 5 characters of the version number.
    git_version = (
        '' if TEST_MODE is not None
        else f'_commit-{fitter.utils.git_latest_version()[7:12]}'
    )

    ye_beta_file = LORENE_YE_BETA.format(
        eos_name=eos_name, commit=git_version)

    outpath = f'../{fitter.consts.OUTPUTDIR}/{eos_name}'
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    fpath = os.path.join(f'{outpath}', ye_beta_file)

    with open(fpath, 'w') as handle:
        np.savetxt(handle, data, fmt)


parser = argparse.ArgumentParser()
parser.add_argument('outdir',
                    help='Location where the eos.* tables are written to '
                    'as a result of the fitting procedure.')
parser.add_argument('config_file',
                    help='Configuration file for the considered equation of '
                    'state to run the fitting procedure.')
args = parser.parse_args()

fit_config = fitter.utils.read_config_file(args.config_file)

# Run the CompOSE software in order to generate the output beta-equilibrated
# versions of the tables, using the corresponding eos.parameters file with
# the beta-equilibrium switch activated.
tables = fitter.compose.CompOSE(
    # No need to download data from CompOSE so set the URL to None
    None,
    args.outdir,
    fit_config.get('eos', 'compose_code')
)

table_path = fit_config.get('eos', 'table_path')
tables.make_tables(
    os.path.join(table_path, 'beta-equil', 'eos.quantities'),
    os.path.join(table_path, 'beta-equil', 'eos.parameters'),
    'eos.table.beta-equil'
)

beta_eq = fitter.eos.EoS(
    os.path.join(args.outdir, 'eos.table.beta-equil'),
    args.outdir
)

pressure_cgs = \
    beta_eq.table['P/MeV fm^-3'] * fitter.consts.MEV_TO_ERG / \
    fitter.consts.FM_TO_CM**3

# Calculate the log-enthalpy. First, get the enthalpy per Baryon for the
# first grid point
particles = beta_eq.read_particle_info()
Mn = particles['mn'][0]

logH = \
    np.log(
        (beta_eq.table['epsilon/MeV fm^-3'] + beta_eq.table['P/MeV fm^-3']) /
        (beta_eq.table['nb/fm^-3'] * Mn)
        )

# Apply a shift to the data.
logH = logH - logH[0] + 1.e-14

log.info('Writing out beta-equilibrated table.')
write_beta_fit(
    beta_eq.table['nb/fm^-3'],
    beta_eq.table['rho/g cm^-3'],
    pressure_cgs, fit_config.get('eos', 'name')
)

log.info('Writing electron charge fraction profile.')
write_ye_beta(logH, beta_eq.table['Y_q'], fit_config.get('eos', 'name'))
