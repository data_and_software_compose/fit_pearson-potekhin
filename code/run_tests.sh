#!/bin/bash
# ---------------------------------------------
#
# Run unit tests using Python's pytest module
#
# SYNOPSIS:
#    ./run_tests.sh
#
# ---------------------------------------------

# Set environment variable to run the scipts in
# test mode. This prevents git commit information
# being written to the output files, that may cause
# the comparisons to break, since the commit versions
# will frequently change.
export TEST_MODE=True

# Temporarily turn off warnings. Check line and branch coverage.
python3 -m pytest -v \
	--cov fitter \
	--cov-branch \
	--cov-report term-missing tests/
