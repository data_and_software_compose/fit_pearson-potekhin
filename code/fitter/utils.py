"""
Module that contains utility functions used for the fitting procedure.

"""
import configparser
import glob
import linecache
import logging as log
import os
import subprocess
import sys

import numpy as np
import pandas as pd
import scipy.integrate
import scipy.interpolate
import scipy.optimize

from fitter import consts


def read_config_file(configfile):
    """
    Read a configuration file that contains the parameters associated
    with the fitting procedure. Check also that the required sections
    are present.

    """
    # TO-DO: Will need updating
    REQUIRED = {
        'eos': [
            'table_path', 'name', 'url', 'compose_code'
        ],

        'fitting': [
            'tolerance', 'method'
        ],

        'nb_model_grid': [
            'nsize', 'start', 'end'
        ],

        'plots': [
            'figdir', 'yq_list', 'ref_table',
            'make_plots'
        ]
    }

    if not os.path.exists(configfile):
        sys.exit(f'File {configfile} does not exist.')

    config = configparser.ConfigParser(
        interpolation=configparser.ExtendedInterpolation())

    config.read(configfile)

    for section in list(REQUIRED.keys()):
        if not config.has_section(section):
            msg = f'Required section "{section}" not found in {configfile}.'
            sys.exit(msg)

        for entry in REQUIRED[section]:
            if not config.has_option(section, entry):
                sys.exit(f'Required entry "{entry}" not in "{section}".')

    return config


def read_lorene_file(filepath, eos_2par, old_col_format=False):
    """
    Reads a two-parameter equation of state table in the Lorene format.
    If old_col_format is True, then we read in a file that contains
    columns that are now redundant.

    :arg: string filepath     : Path to the Lorene file
    :arg: bool eos_2par       : Is True for two-parameter EoS
    :arg: bool old_col_format : Set to true to read in a file containing
                                redundant columns (generated from an older
                                version of the NS hydro code)

    """
    df = pd.read_csv(filepath, delimiter=r'\s+', skiprows=9, header=None)
    if eos_2par:
        if old_col_format:
            df.columns = [
                consts.NB_INDEX, consts.NB_FM3,
                consts.MASS_ENERGY_DENS_G_GM3_LORENE,
                consts.PRESS_DYN_CM2, consts.ENTH_C2,
                consts.YQ, consts.SOUND_SPEED_C2,
                consts.CHISQ,
                consts.MU_LEPT_MEV, consts.D2P_DHDY,
                consts.PROD_RATE
            ]
        else:
            df.columns = [
                consts.NB_INDEX,
                consts.NB_FM3,
                consts.MASS_ENERGY_DENS_G_GM3_LORENE,
                consts.PRESS_DYN_CM2, consts.ENTH_C2, consts.YQ,
                consts.SOUND_SPEED_C2, consts.MU_LEPT_MEV,
                consts.D2P_DHDY,
                consts.PROD_RATE
            ]
    else:
        df.columns = [
            consts.NB_INDEX, consts.NB_FM3,
            consts.MASS_ENERGY_DENS_G_GM3_LORENE, consts.PRESS_DYN_CM2
        ]

    return df


def lorene_grid_size(filepath):
    """
    Get the number of grid points in baryon number density and
    charge fraction in the Lorene file.

    """
    grid_info = linecache.getline(filepath, 6).split()

    # Number of grid points in baryon density and charge fraction
    # respectively.
    return (int(grid_info[0]), int(grid_info[1]))


def search_lorene(table, var, Yq):
    """
    Searches a Lorene file for a variable value according to the
    value of charge fraction, Yq.

    :arg: pandas.DataFrame table : Data frame containing Lorene table
    :arg: str var                : Variable name whose values are to
                                   be searched
    :arg: float Yq               : Search values of var associated with Yq

    """
    # One parameter equations of state only have a single
    # entry for Yq which is zero. Since there is no column
    # in the Lorene file that is associated with Yq for a one
    # parameter EoS, just return the whole column for the
    # requested variable.
    if Yq != 0.e0:
        condition = np.isclose(table[consts.YQ], Yq)
        return table[var][condition].to_numpy()
    else:
        return table[var].to_numpy()


def calc_rel_error(ref_val, test_val):
    """
    Calculate the relative error in per cent between a reference values
    and a test value.

    """
    return 100.e0 * (ref_val - test_val) / ref_val


def check_existing_output(eos_name, outdir='./'):
    """
    Check whether any output files already exist. If so,
    prompt the user to check whether to delete the files and
    continue a new fitting procedure. If deletion is not chosen
    then we abort the code.

    """
    FILES = [
        'eos.nb', 'eos.thermo', 'eos.yq', f'eos_fit*{eos_name}*.d'
        ]

    found_files = []
    for outfile in FILES:
        found_files.extend(glob.glob(os.path.join(outdir, outfile)))

    if found_files:
        file_list = ' '.join(found_files)
        delete = input(f'Found files: {file_list}. Delete? [y/n] \n')
        if delete.lower() == 'y':
            for outfile in found_files:
                log.info(f'Deleting {outfile}.')
                os.remove(outfile)
        else:
            log.info('Keeping files. Aborting fit procedure.')
            sys.exit()


def interp_smooth(x, y, xmin, xmax, interp_type='quadratic'):
    """
    Smooth out data points located within the defined interval
    [xmin, xmax], using the defined interpolation function.
    The interpolation function is constructed using points
    outside of the domain [xmin, xmax].

    :arg: numpy.array x   : Independent variable
    :arg: numpy.array y   : Dependent variable
    :arg: float xmin      : Lower value of the independent variable
                            defining region to be smoothed
    :arg: float xmax      : Upper value of the independent variable
                            defining the region to be smoothed
    :arg: str interp_type : Interpolation type

    """
    # Generate a interpolation function, neglecting the values
    # defined in the sepcified domain. Then replace those values
    # with those calculating from the interpolation.
    condition = (x >= xmin) & (x <= xmax)

    y_masked = np.ma.masked_where(condition, y)
    x_masked = np.ma.masked_where(condition, x)

    # Define the interpolation function, but ignoring those
    # points within the defined range (xmin, xmax)
    finterp = scipy.interpolate.interp1d(
        x[~x_masked.mask], y[~y_masked.mask], interp_type)

    ysmooth = np.where(condition, finterp(x), y)

    return ysmooth


def git_branch_name():
    """Return the name of the git branch where the script is being run."""
    # The checked out branch is indicated by a leading asterisk,
    # (and leading space) which should be removed before being returned
    # by the function.
    output = subprocess.run(
        ["git branch | grep '^*'"], shell=True, capture_output=True,
        text=True
        )

    return output.stdout.strip('* ')


def git_latest_version():
    """Return the current version of the branch where the script is run."""
    output = subprocess.run(
        ["git log -n 1 | grep '^commit'"], shell=True, capture_output=True,
        text=True
        )

    return output.stdout


def write_fit_pars_file(popt, eos_name, fname, test_mode=None):
    """
    Write an output file that stores the best fit parameters
    for the specified equation of state.

    :arg: list popt    : Fitting coefficients
    :arg: str eos_name : Name of the considered equation of state
    :arg: str fname    : Name of the file to write the parameters to

    """
    git_version = (
        '\n' if test_mode is not None
        else git_latest_version()
    )
    git_branch = (
        '\n' if test_mode is not None
        else git_branch_name()
    )

    log.info(f'Writing fit params file: {fname}.')
    fpath = os.path.join(f'../{consts.OUTPUTDIR}/{eos_name}/')
    if not os.path.exists(fpath):
        os.makedirs(fpath)

    fout = os.path.join(f'{fpath}/{fname}')

    with open(fout, 'w') as handle:
        # Add header information specifying the code versions and EoS
        header = (f'# {eos_name}\n'
                  f'# {git_branch}'
                  f'# {git_version}')
        handle.write(header)
        for i, param in enumerate(popt):
            entry = f'p{i+1} = {param:.6f}\n'
            handle.write(entry)


def bisection(x, func, *args, tol=1.e-16):
    """
    Find the root of a function using scipy's brentq algorithm.
    This function automatically re-adjusts the bracketing values
    for the dependent variable if the root is not immediately
    bracketed.

    :arg: numpy.array x  : Array for the independent variable
    :arg: function func  : Function to find the root for
    :arg: list *args     : List of arguments to supply to func
    :arg: float tol      : Tolerance for the solution

    """
    # As an initial guess for the bracketing values, find the
    # minimum and maximum values in the array. Use these to find
    # the corresponding function values.
    xmin = np.min(x)
    xmax = np.max(x)

    if xmin == xmax:
        xmax = xmax * 1.01e0

    bracketed = func(xmin, *args) * func(xmax, *args) < 0.e0
    k = 1
    while not bracketed and k <= 200:

        # If the function values are negative, then shift
        # the higher bracketing value to larger values.
        # Otherwise, shift the lower bracketing value to
        # lower values
        grad = (func(xmax, *args) - func(xmin, *args)) / (xmax - xmin)

        if grad > 0.e0:
            if func(xmax, *args) < 0.e0:
                xmax += k * 1.e-1
            elif func(xmin, *args) > 0.e0:
                xmin /= k * 1.1e0
        elif grad < 0.e0:
            if func(xmax, *args) > 0.e0:
                xmax += k * 1.e-1
            elif func(xmin, *args) < 0.e0:
                xmin /= k * 1.1e0

        bracketed = func(xmin, *args) * func(xmax, *args) < 0.e0
        k += 1

    if not bracketed:
        sys.exit('Bracketing values could not be found.')

    return scipy.optimize.brentq(
        func, xmin, xmax, args=(args), disp=True, xtol=tol)


def remove_pions(p, rho, rhomin, rhomax=8.e14):
    """
    Remove a phase transition in pressure due to the appearance
    of pions in the core. We perform a linear smoothing
    of the adiatic index to remove the phase transition, and
    then we recompute the pressure from the new index by
    integrating from the core to the crust.

    :arg: np.array p   : Pressure in MeV/fm^3
    :arg: np.array rho : Mass-energy density in g/cm^3
    :arg: float rhomin : Search for the phase transition at
                         densities above rhomin (g/cm^3)

    """
    g_to_mev = consts.C_CGS**2 / consts.MEV_TO_ERG

    eps = rho * g_to_mev * consts.FM_TO_CM**3
    gamma = (eps/p) * np.gradient(p, eps, edge_order=2)

    condition = (rho >= rhomin) & (rho <= rhomax)
    gamma_core = gamma[condition]
    rho_core = rho[condition]

    # Proceed towards the core and grab the density value where there
    # is a large jump in the index. Then proceed from the core inwards
    # to find the upper limit in rho.
    # TO-DO: Make the 0.03 an argument to the function
    for i in range(len(gamma_core)-1):
        if np.abs(gamma_core[i+1] - gamma_core[i]) >= 0.03:
            rhomin = rho_core[i+1]
            break

    for i in range(len(gamma_core)-1, 1, -1):
        if np.abs(gamma_core[i-1] - gamma_core[i]) >= 0.03:
            rhomax = rho_core[i-1]
            break

    gamma = interp_smooth(
        rho, gamma, rhomin, rhomax, interp_type='linear')

    # Integrate from the core back towards the crust, to ensure that
    # core values are faithful to the original table, until at least
    # to where the phase transition has been smoothed over.
    logP0 = np.log(p[-1])
    logP = []
    logP.append(logP0)

    log.info('Recomputing pressure.')
    for i in range(1, len(gamma)):
        logP.append(
            logP0 + scipy.integrate.simpson(
                gamma[-1:-i-2:-1]/eps[-1:-i-2:-1], eps[-1:-i-2:-1]
                )
        )

    return np.exp(logP[-1::-1])
