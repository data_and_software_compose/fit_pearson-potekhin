"""
This module contains the procedure to attach a polytropic crust to the
fitted core, ensuring continuity in pressure, pressure gradient
and energy density. Thermodynamic quantities are then calculated
throughout the whole neutron star domain.

"""
import functools
import os
import sys
import logging as log

import dask
import numpy
import scipy.integrate
import scipy.interpolate
import scipy.optimize

import fitter.consts
import fitter.utils

from fitter import (pearson, utils)
from fitter.consts import (
    C_CGS, FM_TO_CM, m0_MeV, eps_convert, msol_geom, si_to_geom, cgs_to_si,
    MEV_FMCUBED_TO_ERG_CMCUBED, PRESS_MEV_FM3, MASS_ENERGY_DENS_G_CM3,
    MEV_TO_ERG
    )


deriv_pearson = pearson.model_core_deriv()

TEST_MODE = os.getenv('TEST_MODE')


# The next two functions need implementing as a sanity check
def has_nans(array):
    """
    Check whether any of the elements in the supplied array
    has NaNs.

    """
    return numpy.any(numpy.isnan(array))


def has_infs(array):
    """
    Check whether any of the elements in the supplied array
    has infinities.

    """
    return numpy.any(numpy.isinf(array))


def dlognb_drho(density, lognB, Yq, pars, fitfunc):
    """
    Define the derivative of the (natural) log of the baryon number
    density with respect to mass-energy density. The argument order is
    required by scipy's solve_ivp routine which expects the
    independent variable to come first. Evaluate the pressure from a
    specified fit function.

    :arg: float density : Mass-energy density (g/cm^3)
    :arg: float lognB   : Natural log of the baryon density (1/cm^3)
    :arg: float Yq      : Electron fraction (adim)
    :arg: list pars     : Fitting parameters
    :arg: func fitfunc  : Pearson fit function (either 1- or 2-param scheme)

    """
    logrho = numpy.log10(density)

    # TO DO: mesh could be just replaced by logrho
    mesh = logrho

    logP = fitfunc(mesh, *pars, cgs=True)
    press = 10.e0**logP

    return C_CGS**2 / ((density*C_CGS**2) + press)


def calc_baryon_num_density(rho0, rho, lognB0, pars, Yq, rho_eval,
                            fitfunc):
    """
    Calculate the Baryon number density in 1/fm^3.

    :arg: float rho0         : Lower integration bound for the
                               mass-energy density (g/cm^3)
    :arg: float rho          : Upper integration bound for the
                               mass-energy density (g/cm^3)
    :arg: float lognB0       : Initial value of the log of the
                               baryon density (1/cm^3)
    :arg: list pars          : Fitting coefficients
    :arg: float Yq           : Charge fraction (adim)
    :arg: np.array rho_eval  : Values of mass-energy density for
                               which we want to solve for
    :arg: func fitfunc       : Fit function

    """
    solver = scipy.integrate.solve_ivp(
        dlognb_drho, (rho0, rho), [lognB0],
        t_eval=rho_eval, method='DOP853', args=(Yq, pars, fitfunc),
        )

    nb = numpy.exp(solver.y.flatten()) * FM_TO_CM**3
    return nb


def solve_for_rho(logrho, logrho0, lognB0, pars, Yq, nBref,
                  fitfunc):
    """
    This is used in conjunction with a bisection routine to
    solve for the mass-energy density for a given value of
    the baryon density.

    :arg: float logrho  : log10 of the guess value for mass-
                          energy density (g/cm^3)
    :arg: float logrho0 : log10 of the lower integration limit
                          for the mass-energy density (g/cm^3)
    :arg: float lognB0  : Initial value of the natural log of
                          the baryon density (1/cm^3)
    :arg: list pars     : Fitting coefficients
    :arg: float Yq      : Electron charge fraction (adim)
    :arg: float nBref   : Value of baryon num density to solve
                          for (1/fm^3)
    :arg: func fitfunc  : Fitting function between log10 pressure
                          and log10 mass-energy density

    """
    rho0 = 10.e0**logrho0
    rho = 10.e0**logrho

    rho_eval = [rho0, rho]

    return calc_baryon_num_density(
        rho0, rho, lognB0, pars, Yq, rho_eval, fitfunc)[-1] - nBref


def _calc_rho(eos_table, iYq, temp, fitfunc, fitpars, rho0, nB):
    """
    For a given value of the baryon number density and a given slice
    along the electron charge fraction, solve for the mass-energy
    density. Use a bisection method to solve the "solve_for_rho"
    function.

    :arg eos.EoS eos_table : Instance of the eos.EoS class containing
                             EoS table information
    :arg: integer iYq      : Slice number for the charge fraction
    :arg: float temp       : Temperature [MeV]
    :arg: func fitfunc     : Fit function between pressure and mass-energy
                             density
    :arg: list fitpars     : Best fit coefficients
    :arg: float rho0       : First grid point in mass-energy density (g/cm^3)
    :arg: float nB         : Baryon number density (1/fm^3)

    """
    # TO-DO: mesh can just be replaced with rho0
    mesh = numpy.log10(rho0)

    # Calculate the initial value of the baryon density via the first
    # value of enthalpy per baryon, mass-energy density and the
    # pressure from the fits for the first grid point in the fitting
    # window.
    h0 = eos_table.search(
        'H/erg', temp, eos_table.Ygrid[iYq], eos_table.NBgrid[0])[0]

    P0 = 10.0**fitfunc(mesh, *fitpars, cgs=True)
    nb0 = (rho0*C_CGS**2 + P0) / h0

    lognb0 = numpy.log(nb0)

    # If we are dealing with a one parameter equation of state,
    # then we don't need to worry about the electron charge fraction
    # so set this to none

    # TO-DO: Since we are applying P(\rho) Pearson fit for
    # each individual slice along Yq, this can be removed
    # (along with Yq args for calc_baryon_num_density and
    # dlognb_drho
    if eos_table.Ygrid[iYq] == 0.e0:
        Ye = None
    else:
        Ye = eos_table.Ygrid[iYq]

    # Calculate a crude guess value for the mass-energy
    # density from the baryon number density and mass of
    # a baryon.
    logrho_guess = numpy.log10(nB * m0_MeV * eps_convert)

    logrho0 = numpy.log10(rho0)
    logrho = utils.bisection(
        logrho_guess, solve_for_rho, logrho0,
        lognb0, fitpars, Ye, nB,
        fitfunc)

    return 10.e0**logrho

# TO-DO Needs tidying up and possibly replacing with multiprocessing
# as may not always be relaible.
@dask.delayed
def calc_rho_dask(eos_table, iYq, temp, fitfunc, fitpars, rho0, nB):
    """
    Use dask to calculate mass-energy density for multiple
    grid points in parallel.

    """
    return _calc_rho(eos_table, iYq, temp, fitfunc, fitpars, rho0, nB)


def calc_rho_wrapper(eos_table, iYq, temp, fitfunc, fitpars, rho0, nbarray):
    """
    Calculate the mass-energy density for multiple grid points in parallel
    using the Dask library.

    """
    results = []
    for nB in nbarray:
        results.append(
            calc_rho_dask(eos_table, iYq, temp, fitfunc, fitpars, rho0, nB)
        )

    out = dask.delayed(results)

    with dask.config.set(scheduler='threads'):
        rhogrid = out.compute()

    return numpy.array(rhogrid)


def dPdn_pearson(nB, pars, eos_table, temp, fitfunc, rho0, iYq=0,
                 fit_deriv=deriv_pearson):
    """
    Calculate the derivative of the pressure w.r.t the baryon number density
    for the Potekhin-Pearson analytical fit.

    :arg: float nB          : Baryon number density (1/fm^3)
    :arg: list pars         : Best fit parameters
    :arg: eos.EoS eos_table : Instance of eos.EoS class containing EoS tables
    :arg: float temp        : Temperature (MeV)
    :arg: func fitfunc      : Analytical fit function
    :arg: float rho0        : First grid point for mass-energy density in
                              fitting window (g/cm^3)
    :arg: int iYq           : Slice index along electronic charge fraction
    :arg: func fit_deriv    : Derivative of the log10 pressure (dyn/cm^2)
                              w.r.t log10 of the mass-energy density (g/cm^3)

    """
    density = _calc_rho(eos_table, iYq, temp, fitfunc, pars, rho0, nB)
    density = numpy.array(density)

    logP = fitfunc(numpy.log10(density), *pars, cgs=True)
    press = 10.e0**logP

    nB_cgs = nB / FM_TO_CM**3
    drho_dn = (press + (density*C_CGS**2)) / (nB_cgs*C_CGS**2)
    dzeta_dxi = fit_deriv(numpy.log10(density), *pars)
    dPdn = (press / density) * dzeta_dxi * drho_dn

    return dPdn


def pressure_polytrope(nB, kappa, gamma, Lambda=0.e0):
    """
    Calculate the polytropic pressure

    :arg: float nB     : Baryon density
    :arg: float kappa  : Polytropic constant
    :arg: float gamma  : Polytropic index
    :arg: float Lambda : Boundary condition if the generalised
                         piece-wise polytrope scheme is used.

    """
    return kappa*nB**gamma + Lambda


def energy_density_polytrope(nB, kappa, gamma, mu0, Lambda=0.e0):
    """
    Calculate the energy density based on a polytropic model
    :arg: float nB     : Baryon number density
    :arg: float kappa  : Polytropic constant
    :arg: float gamma  : Polytropic index
    :arg: float mu0    : chemical potential at zero pressure
    :arg: float Lambda : Boundary condition if generalised
                         peice-wise polytrope scheme is used

    """
    gamma_minus_one = gamma - 1.e0
    factor = kappa * nB**gamma / gamma_minus_one
    return factor + mu0*nB - Lambda


def energy_density(rho, nB, Lambda, d):
    """
    Energy density, given a value of the mass-energy density,
    using the generalised peice-wise polytrope (GPP) scheme.

    :arg: float rho    : Mass-energy density
    :arg: float nB     : Baryon density
    :arg: float Lambda : Boundary condition for the GPP scheme
    :arg: float d      : Boundary condition for the GPP scheme

    """
    return (rho*C_CGS**2) + (1.e0+d) * \
        (nB/FM_TO_CM**3) - Lambda


def dPdn_polytrope(nB, kappa, gamma):
    """
    Derivative of the pressure w.r.t the baryon number
    density for a polytropic model.

    :arg: float nB    : Baryon number density
    :arg: float kappa : Polytropic constant
    :arg: float gamma : Polytropic index

    """
    return kappa*gamma*nB**(gamma - 1.e0)


def kappa_geom_to_rel_units(kappa, gamma):
    """
    Convert a value of kappa given in geometrical units into
    relativistic units

    """
    kappa = kappa * \
        msol_geom**(2.*(gamma-1.))/si_to_geom(gamma)/cgs_to_si(gamma)

    kappa = kappa * (m0_MeV * eps_convert)**gamma

    return kappa / MEV_FMCUBED_TO_ERG_CMCUBED


class CrustCoreModel:
    """
    Class that describes the structure of a neutron star.
    The structure consists of three components: the core which is
    described by the Pearson fitting scheme, a polytropic crust, and
    an intermediate zone between the two. The three regions are stitched
    together via a Generalised Piece-wise polytrope scheme to ensure that
    the sound speed is continuous across the boundaries.

    """
    def __init__(self, eos_name, eos_table, Ptab, rhotab, fitfunc, kappa,
                 gamma, nlim1, nlim2, tmin, rho0, iYq, nbgrid,
                 eos_model, guess_params, Yq, mintol, minmethod):
        """
        Class constructor giving the parameters describing the core,
        crust and the stitching regions.

        :arg: str eos_name           : Name of the input equation of state
        :arg: eos.EoS eos_table      : Object containing EoS tables
        :arg: np.array Ptab          : Tabulated values of the pressure
                                       (MeV/fm^3)
        :arg: np.array rhotab        : Tabulated values of mass-energy density
                                       (g/cm^3)
        :arg: func fitfunc           : Analytical fitting function
        :arg: float kappa            : Polytropic constant for the crust (MeV)
        :arg: float gamma            : Polytropic index for the crust (adim)
        :arg: float nlim1            : Baryon number density where the
                                       polytropic crust and intermediate zone
                                       are stitched (1/fm^3)
        :arg: float nlim2            : Baryon number density where the
                                       intermediate zone and core are stitched
                                       together (1/fm^3)
        :arg: float tmin             : First temperature entry of equation of
                                       state (MeV)
        :arg: float rho0             : Initial value of mass-energy density in
                                       the fitting window (g/cm^3)
        :arg: int iYq                : Slice number along the electron charge
                                       fraction (adim)
        :arg: np.array nbgrid        : Grid values of the baryon num density to
                                       calculate thermodynamic quantities
                                       for (1/fm^3)
        :arg: PearsonModel eos_model : Object associated with the Pearson fit
                                       formalism
        :arg: list guess_params      : Initial guesses for the fit coefficients
        :arg: float Yq               : Value of electronic charge fraction to
                                       perform fit for
        :arg: float mintol           : Tolerance for the minimization procedure
                                       to add crust
        :arg: str minmethod          : Minimization method to use

        """
        self.eos_name = eos_name
        self.eos_table = eos_table
        self.Ptab = Ptab
        self.rhotab = rhotab
        self.fitfunc = fitfunc
        self.kappa = kappa
        self.gamma = gamma
        self.nlim1 = nlim1
        self.nlim2 = nlim2
        self.tmin = tmin
        self.rho0 = rho0
        self.iYq = iYq
        self.nbgrid = nbgrid
        self.eos_model = eos_model
        self.guess_params = guess_params
        self.Yq = Yq
        self.mintol = mintol
        self.minmethod = minmethod

        self.Mn = self.eos_table.particles['mn'].to_numpy()[0]
        self.nlim1_cgs = self.nlim1 / FM_TO_CM**3
        self.nlim2_cgs = self.nlim2 / FM_TO_CM**3

        # Initial guess values for kappa and gamma for the
        # intermediate region.
        self.kappa0 = self.kappa
        self.gamma0 = self.gamma

        # Define, respectively, the intervals in baryon number density
        # for the polytropic crust, the intermediate zone and
        # core. Needed so that the appropriate functions can be used
        # to calculate the pressure and densities in each zone.
        self.condlist = [
            self.nbgrid <= self.nlim1,
            numpy.logical_and(
                self.nbgrid > self.nlim1,
                self.nbgrid <= self.nlim2
            ),
            self.nbgrid > self.nlim2
        ]

    def fitter(self, rho, Lambda, fitpars):
        """
        Calculate the pressure using the Potekhin-Pearson scheme, accounting
        for the boundary condition, Lambda, to ensure continuity with
        the intermediate region.

        """
        Ppearson = 10.e0**self.fitfunc(
            numpy.log10(rho), *self.fitpars, cgs=True
        )

        return Ppearson + Lambda

    def funcsolve(self, x):
        """
        Define the system of equations that describe pressure continuity
        between each of the three zones, in order to calculate the values
        of kappa and gamma for the intermediate zone.

        :arg: list x : Contains values of kappa, x[0], and gamma, x[1] of
                       the intermediate region.

        """
        conversion = MEV_FMCUBED_TO_ERG_CMCUBED * FM_TO_CM**3
        return [
            dPdn_polytrope(self.nlim1, x[0], x[1]) * conversion -
            dPdn_polytrope(self.nlim1, self.kappa, self.gamma) * conversion,
            dPdn_polytrope(self.nlim2, x[0], x[1]) * conversion -
            dPdn_pearson(self.nlim2, self.fitpars, self.eos_table,
                         self.tmin, self.fitfunc, self.rho0)
        ]

    @property
    def intermediate_boundary_pars(self):
        """
        Calculate the values of kappa, gamma and then d and Lambda
        for the intermediate zone linking the crust and core.

        """
        # TO-DO tidy up and rationalise the log output
        # Get the values of kappaGPP and gammaGPP for the intermediate zone
        print('GPP', self.fitpars)
        par0 = [self.kappa0, self.gamma0]

        roots = scipy.optimize.root(self.funcsolve, x0=par0)
        print(roots)

        kappaGPP = roots.x[0]
        gammaGPP = roots.x[1]

        self.kappa0 = kappaGPP
        self.gamma0 = gammaGPP

        # Calculate the value of Lambda for the intermediate zone via
        # the crust pressure, and the pressure in the intermediate zone
        # at the lower boundary nBlim1.
        Pclim1 = pressure_polytrope(self.nlim1, self.kappa, self.gamma) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        PGPP1 = pressure_polytrope(self.nlim1, kappaGPP, gammaGPP) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        LambdaGPP = Pclim1 - PGPP1

        print('Pclim1', Pclim1)
        print('PGPP1', PGPP1)

        # Calculate the value of dGPP from the energy density of the crust
        # and the energy density at the intermediate zone at the lower
        # boundary nBlim1.
        enlim1 = energy_density_polytrope(
            self.nlim1, self.kappa, self.gamma, self.Mn) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        eGPP1 = energy_density_polytrope(
            self.nlim1, kappaGPP, gammaGPP, 0.0) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        dGPP = (1.e0 / self.nlim1_cgs) * (enlim1 + LambdaGPP - eGPP1) - \
            1.e0

        print(kappaGPP, gammaGPP, LambdaGPP, dGPP)
        return kappaGPP, gammaGPP, LambdaGPP, dGPP

    @property
    def core_boundary_pars(self):
        """
        Calculate the values of Lambda and d by ensuring continuity in
        pressure and energy at the upper boundary nBlim2, between the
        core and the intermediate zone.

        """
        kappaGPP, gammaGPP, LambdaGPP, dGPP = \
            self.intermediate_boundary_pars

        # Pressure in the intermediate zone at the upper boundary
        # nBlim2
        PGPP2 = pressure_polytrope(
            self.nlim2, kappaGPP, gammaGPP,
            LambdaGPP/MEV_FMCUBED_TO_ERG_CMCUBED) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        print('PGPP2', PGPP2)

        # Get the mass-energy density at the upper boundary
        density = self.rho(self.fitpars, self.nlim2)
        density = numpy.array(density)

        print(f'rho(nlim2): {density}')

        # TO-DO: just replace with numpy.log10(density)
        # Calculate the pressure using the Pearson scheme at the
        # upper boundary nBlim2 in order to calculate Lambda
        mesh = numpy.log10(density)

        logP = self.fitfunc(mesh, *self.fitpars, cgs=True)
        Pcore2 = 10.e0**logP

        print('Ppear(nlim2): ', Pcore2)

        Lambda = PGPP2 - 10.e0**logP
        print('Lambda: ', Lambda, PGPP2 - 10.**logP)

        # Now for d, via the energy density in the core and in the
        # intermediate region at the upper boundary nBlim2,
        # respectively.
        ecore2 = self.rho(self.fitpars, self.nlim2) * C_CGS**2

        eGPP2 = energy_density_polytrope(
            self.nlim2, kappaGPP, gammaGPP, 0.0) * \
            MEV_FMCUBED_TO_ERG_CMCUBED

        d = (1.e0 / self.nlim2_cgs) * \
            (eGPP2 + (1.+dGPP) * self.nlim2_cgs - LambdaGPP + Lambda -
             ecore2) - 1.e0

        print(d)

        return Lambda, d

    def residuals(self, minopt):
        """
        Define the sum of the residuals between the pressure from the
        Potekhin-Pearson scheme and the tabulated values. This is used
        by the minimization procedure when adding the crust.

        :arg: list minopt : Optimal fitting coefficients in the
                            Potekhin-Pearson scheme, calculated during
                            the minimization procedure.

        """
        sumres = 0.e0

        # Update the best fit coefficients (calculated when Lambda = 0)
        # with those calculated during the minimization.
        self.fitpars = minopt

        Lambda, _ = self.core_boundary_pars

        # Define grid and grid width in log of mass-energy density needed
        # for the cost function
        logrho_width = numpy.log10(self.rhotab[-1]) - \
            numpy.log10(self.rhotab[0])

        dlogrho = [numpy.log10(self.rhotab[i+1]) - numpy.log10(self.rhotab[i])
                   for i in range(len(self.rhotab)-1)]

        # Define the grid-point distance in logrho associated with the
        # final point in rho.
        dlogrho.append(
            numpy.log10(self.rhotab[-1]) - numpy.log10(self.rhotab[-2])
        )

        for i in range(len(self.rhotab)):
            logPfit = numpy.log10(self.fitter(self.rhotab[i], Lambda, minopt))
            logPtab = numpy.log10(self.Ptab[i] * MEV_FMCUBED_TO_ERG_CMCUBED)

            sumres += dlogrho[i]*(logPfit - logPtab)**2

        sumres = sumres / logrho_width
        print('sumres:', sumres)

        relerror = numpy.abs(utils.calc_rel_error(
            self.Ptab * MEV_FMCUBED_TO_ERG_CMCUBED,
            self.fitter(self.rhotab, Lambda, minopt)
        ))

        print('Min error: ', numpy.min(relerror))
        print('Max error: ', numpy.max(relerror))
        print('Mean error: ', numpy.mean(relerror))

        return sumres

    def perform_init_fit(self):
        """
        Carry out an initial fit to the core, with Lambda = 0.  The
        resulting fit coefficients will be used as an initial guess
        for the minimization procedure.

        """
        popt = self.eos_model.fit(self.guess_params)
        Pbest = self.eos_model.bestfit(popt, self.rhotab)
        relerr = numpy.abs(
            utils.calc_rel_error(self.Ptab, Pbest)
            )

        log.info(f'Min. rel. err: {numpy.min(relerr):.5e} %')
        log.info(f'Max. rel. err: {numpy.max(relerr):.5e} %')
        log.info(f'Avg. rel. err: {numpy.mean(relerr):.5e} %')

        fname = f'init_fit_parameters_{self.eos_name}_Yq_{self.Yq:.4f}.dat'
        utils.write_fit_pars_file(
            popt, self.eos_name, fname, TEST_MODE)

        return popt

    def optimize(self):
        """
        Run the mimimization procedure to add the crust, and
        update the best fit coefficients of the Potekhin-
        Pearson formalism.

        """
        minopt = scipy.optimize.minimize(
            self.residuals, x0=self.fitpars, tol=self.mintol,
            bounds=self.bounds, method=self.minmethod
        )

        print(minopt)
        self.fitpars = minopt.x
        return minopt

    def run(self, popt):
        """
        Wrapper function that performs an initial fit to the
        core, and then runs the mimization procedure to add
        the crust model, optimizing the fit coefficients along
        the way.

        """
        # Run an initial fit if a set of fit parameters
        # have not yet been supplied. Bypassing this
        # step may be useful for the two-parameter scheme
        # where we can use the optimum paremeters from
        # the minimization scheme of the previous slice in Yq.
        if popt is None:
            self.fitpars = self.perform_init_fit()
        else:
            self.fitpars = popt

        # Set bounds to the coefficients associated with the gradient
        # in the linear factor in the first, second and third terms of
        # the expression used to fit the core. This was done by trial-
        # and-error to avoid problems during the minimization.
        self.bounds = (
            (None, None), (None, None),
            (self.fitpars[2], self.fitpars[2]), (None, None),
            (None, None), (self.fitpars[5], self.fitpars[5]),
            (None, None), (None, None),
            (None, None), (self.fitpars[9], self.fitpars[9]),
            (None, None), (None, None), (None, None), (None, None),
            (None, None)
        )
        self.optimize()

    def rho(self, fitpars, n):
        """
        Wrapper function to calculate mass-energy density for a given set
        of fit parameters and baryon density (1/fm^3)

        """
        return _calc_rho(
            self.eos_table, self.iYq, self.tmin,
            self.fitfunc, fitpars, self.rho0, n
        )

    @functools.lru_cache
    def rho_core(self, fitpars):
        """
        Calculate the values of mass-energy density just for the
        core region.

        """
        return calc_rho_wrapper(
            self.eos_table, self.iYq, self.tmin,
            self.fitfunc, fitpars, self.rho0, self.nbgrid
        )

    @functools.cached_property
    def pressure(self):
        """
        Calculate the pressure (dyn/cm^2) for the crust, intermediate
        region and within the core. Define functions to calculate the
        pressure for each of these regions.

        """
        kappaGPP, gammaGPP, LambdaGPP, dGPP = \
            self.intermediate_boundary_pars
        Lambda, d = \
            self.core_boundary_pars

        funclist = [
            pressure_polytrope(
                self.nbgrid, self.kappa, self.gamma) *
            MEV_FMCUBED_TO_ERG_CMCUBED,
            pressure_polytrope(
                self.nbgrid, kappaGPP, gammaGPP,
                LambdaGPP/MEV_FMCUBED_TO_ERG_CMCUBED) *
            MEV_FMCUBED_TO_ERG_CMCUBED,
            self.fitter(
                self.rho_core(tuple(self.fitpars)),
                Lambda, self.fitpars)
        ]

        return numpy.select(self.condlist, funclist)

    @functools.cached_property
    def mass_energy_density(self):
        """
        Calculate the mass energy density (g/cm^3) for the crust,
        intermediate zone and the core. Functions are defined to
        calculate this for each region.

        """
        kappaGPP, gammaGPP, LambdaGPP, dGPP = \
            self.intermediate_boundary_pars
        Lambda, d = \
            self.core_boundary_pars

        one_plus_dGPP = (1.e0 + dGPP) / MEV_TO_ERG

        funclist = [
            energy_density_polytrope(
                self.nbgrid, self.kappa, self.gamma, self.Mn) *
            MEV_FMCUBED_TO_ERG_CMCUBED,
            energy_density_polytrope(
                self.nbgrid, kappaGPP, gammaGPP, one_plus_dGPP,
                LambdaGPP/MEV_FMCUBED_TO_ERG_CMCUBED) *
            MEV_FMCUBED_TO_ERG_CMCUBED,
            energy_density(
                self.rho_core(tuple(self.fitpars)),
                self.nbgrid, Lambda, d)
        ]

        return numpy.select(self.condlist, funclist) / C_CGS**2

    @property
    def free_energy_density(self):
        """Free energy density in MeV/fm^3"""
        mev_to_g = MEV_TO_ERG / C_CGS**2
        return self.mass_energy_density * \
            FM_TO_CM**3 / mev_to_g

    @functools.cached_property
    def scaled_free_energy(self):
        """Scaled free energy density (adim)"""
        return (
            self.free_energy_density / (self.Mn * self.nbgrid)
            ) - 1.e0

    @property
    def scaled_internal_energy(self):
        """Scaled internal energy (adim). Same as scaled free energy
        for cold matter."""
        return self.scaled_free_energy

    @functools.cached_property
    def lepton_chemical_potential(self):
        """Lepton chemical potential for cold NS matter."""
        return numpy.full(len(self.nbgrid), 0.e0)


class FitTable:
    """
    Class for calculating thermodynamic quantities for
    a given equation of state, once the crust polytropic
    model has been attached to the fitted core.

    """
    def __init__(self, eos_name, eos_table, fitfunc, kappa, gamma,
                 nBlim1, nBlim2, tmin, new_Nb, guess_pars,
                 xtol, fit_method, mintol, minmethod):
        """
        Class constructor

        :arg: eos_name          : Name of the input equation of state
        :arg: eos.EoS eos_table : Object containing EoS tables
        :arg: func fitfunc      : Analytical fitting function
        :arg: float kappa       : Polytropic constant for the crust (MeV)
        :arg: float gamma       : Polytropic index for the crust (adim)
        :arg: float nBlim1      : Baryon number density where the polytropic
                                  crust and intermediate zone are stitched
                                  (1/fm^3)
        :arg: float nBlim2      : Baryon number density where the intermediate
                                  zone and core are stitched together (1/fm^3)
        :arg: float tmin        : First temperature entry of equation of state
                                  (MeV)
        :arg: np.array new_Nb   : Grid of baryon number densities to calculate
                                  thermodynamic quantities for (1/fm^3)
        :arg: list guess_pars   : Initial guesses for the fit coefficients
        :arg: float xtol        : Tolerance for the least-squares fit using
                                  the Potekhin-Pearson scheme
        :arg: str fit_method    : Method to use for the least-squares fit
        :arg: float mintol      : Tolerance for the mimimization procedure
        :arg: str minmethod     : Method to use for the minimization procedure

        """
        self.eos_name = eos_name
        self.eos_table = eos_table
        self.fitfunc = fitfunc
        self.kappa = kappa
        self.gamma = gamma
        self.nBlim1 = nBlim1
        self.nBlim2 = nBlim2
        self.tmin = tmin
        self.new_Nb = new_Nb
        self.guess_pars = guess_pars
        self.xtol = xtol
        self.fit_method = fit_method
        self.mintol = mintol
        self.minmethod = minmethod

        self.Mn = self.eos_table.particles['mn'].to_numpy()[0]
        self.models = []

    def make(self):
        """
        Attach a polytropic crust model to a fit of the core
        for each slice along the electronic charge fraction.
        Returns the converged model via an instance of CrustCoreModel
        for each slice.

        """
        prevpopt = None

        if self.eos_name == 'SROAPR':
            log.info('SROAPR: Removing phase transition associated to pions.')
            self.eos_table.remove_pions(self.tmin)

        for iY, Yq in enumerate(self.eos_table.Ygrid):
            print(f'Adding crust for Yq = {Yq}')

            if Yq == 0.e0:
                # This is a cold equation of state, so just use the
                # the constant value sepcified in the configuration file.
                # TO-DO, for consistency with 2-par case, supply kappa in
                # geometric units in config filer and convert to MeV here.
                kappa = kappa_geom_to_rel_units(self.kappa, self.gamma)
            else:
                # Calculate the value of kappa from the value
                # of Ye, via a Polynomial fit. Kappa here is used
                # to specify the fitting coefficients to use.
                coeffs = getattr(fitter.consts, self.kappa)
                kappafunc = numpy.poly1d(coeffs)
                kappa = kappafunc(Yq)
                kappa = kappa_geom_to_rel_units(kappa, self.gamma)

            Ptab = self.eos_table.search(
                PRESS_MEV_FM3, self.tmin, Yq)
            rhotab = self.eos_table.search(
                MASS_ENERGY_DENS_G_CM3, self.tmin, Yq)

            if numpy.any(Ptab < 0.e0):
                sys.exit('Negative pressure found in the EoS table.')

            pearson_fit = pearson.PearsonModel(
                numpy.log10(rhotab), numpy.log10(Ptab),
                self.eos_table.Ygrid, xtol=self.xtol,
                fit_method=self.fit_method)

            model = CrustCoreModel(
                self.eos_name, self.eos_table, Ptab, rhotab,
                pearson.model_core, kappa, self.gamma, self.nBlim1,
                self.nBlim2, self.tmin, rhotab[0],
                iY, self.new_Nb, pearson_fit, self.guess_pars, Yq,
                self.mintol, self.minmethod)

            model.run(prevpopt)

            prevpopt = model.fitpars
            self.models.append(model)

        return self.models

    def write_fit_pars(self):
        """
        Write the crust, intermediate and core fitting parameters.

        """
        coreparfmt = 'fit_parameters_{eos_name}_Yq_{Yq:.4f}.dat'
        crustparfmt = 'crust_parameters_{eos_name}.dat'

        crustcols = (
            '{0:>5}{1:>20}{2:>20}{3:>20}{4:>20}{5:>20}{6:>20}{7:>20}'
            '{8:>20}{9:>20}{10:>20}'.format(
                'Y_e', 'kappa [MeV]', 'gamma', 'nBlim1 [fm^{-3}]',
                'nBlim2 [fm^{-3}]', 'kappaGPP [MeV]', 'gammaGPP',
                'LambdaGPP', 'dGPP', 'Lambda ', 'd\n')
        )

        crustfile = crustparfmt.format(eos_name=self.eos_name)
        crustpath = os.path.join(
            f'../{fitter.consts.OUTPUTDIR}/{self.eos_name}/{crustfile}')
        with open(crustpath, 'w') as handle:
            handle.write(crustcols)

        for model in self.models:
            corefile = coreparfmt.format(
                eos_name=self.eos_name, Yq=model.Yq)
            utils.write_fit_pars_file(
                model.fitpars, self.eos_name, corefile, TEST_MODE)

            kappaGPP, gammaGPP, LambdaGPP, dGPP = \
                model.intermediate_boundary_pars

            Lambda, d = model.core_boundary_pars
            with open(crustpath, 'a') as handle:
                data = [
                    model.Yq, model.kappa, model.gamma, self.nBlim1,
                    self.nBlim2, kappaGPP, gammaGPP, LambdaGPP, dGPP,
                    Lambda, d]

                numpy.savetxt(handle, numpy.column_stack(data), '%24.16E')

    @functools.cached_property
    def pressure(self):
        """
        Generate an array of pressure values (dyn/cm^2) for each slice
        along the electronic charge fraction.
        TO-DO: needs optimizing as very slow

        """
        return numpy.array([model.pressure for model in self.models])

    @functools.cached_property
    def mass_energy_density(self):
        """
        Calculate an array of mass-energy density (g/cm^3)
        for each slice of the electronic charge fraction.

        """
        return numpy.array(
            [model.mass_energy_density for model in self.models])

    @functools.cached_property
    def scaled_free_energy(self):
        """
        Calculate an array of scaled free energy (adim)
        for each slice of the electronic charge fraction.

        """
        return numpy.array(
            [model.scaled_free_energy for model in self.models])

    @functools.cached_property
    def scaled_internal_energy(self):
        """
        Calculate an array of scaled internal energy (adim)
        for each slice of the electronic charge fraction.

        """
        return numpy.array(
            [model.scaled_internal_energy for model in self.models])

    @property
    def lepton_chemical_potential(self):
        """
        Calculate the lepton chemical potential (MeV) for each
        slice of the electronic charge fraction.

        """
        if len(self.eos_table.Ygrid) == 1:
            # Cold NS matter
            return numpy.array(
                [model.lepton_chemical_potential for model in self.models])
        else:
            # General purpose EoS at the lowest temperature entry
            return numpy.gradient(
                self.scaled_free_energy, self.eos_table.Ygrid,
                axis=0, edge_order=2) * self.Mn

    @property
    def free_energy_density(self):
        """
        Calculate the free energy density (MeV/fm^3) throughout the
        neutron star via the mass-energy density (g/cm^3).

        """
        mev_to_g = MEV_TO_ERG / C_CGS**2
        return numpy.array(
            [
                rho * FM_TO_CM**3 / mev_to_g
                for rho in self.mass_energy_density
            ]
        )

    @property
    def square_of_sound_speed(self):
        """Calculate the square of the sound speed (c^2)."""
        return numpy.array([
            numpy.gradient(pressure, energy_density, edge_order=2)
            for (pressure, energy_density)
            in zip(self.pressure/MEV_FMCUBED_TO_ERG_CMCUBED,
                   self.free_energy_density)
            ])

    @property
    def log_enthalpy(self):
        """
        Calculate the log enthalpy (c^2) throughout the Neutron star,
        for a given grid in the Baryon number density.

        """
        pressure = self.pressure / MEV_FMCUBED_TO_ERG_CMCUBED
        return numpy.log(
            (self.free_energy_density + pressure) /
            (self.Mn * self.new_Nb)
            )

    @property
    def d2p_dlogHdYq(self):
        """
        Calculate the second derivative of the pressure with respect
        to the log enthalpy and the charge fraction (dyn cm^-2 c^-2).

        """
        dPdlogH = (
            self.pressure +
            self.free_energy_density * MEV_FMCUBED_TO_ERG_CMCUBED
        )

        d2p_dlogHdYq = numpy.gradient(
            dPdlogH, self.eos_table.Ygrid, axis=0, edge_order=2)

        if has_nans(d2p_dlogHdYq) or has_infs(d2p_dlogHdYq):
            msg = 'd2p_dlogHYq array has NaNs or infinities.'
            sys.exit(msg)

        return d2p_dlogHdYq

    @property
    def neutrino_prod_rate(self):
        """
        Neutrino production rate. Assumed to be zero for
        cold NS matter.

        """
        return numpy.zeros((len(self.eos_table.Ygrid), len(self.new_Nb)))
