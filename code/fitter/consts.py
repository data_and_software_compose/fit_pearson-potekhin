"""
Module to contain constants used throughout the code

"""
import os

import numpy as np

TEST_MODE = os.getenv('TEST_MODE')
if TEST_MODE is not None:
    OUTPUTDIR = 'test_output'
else:
    OUTPUTDIR = 'outputs'

# Physical constants, taken from 2018 CODATA recommended values
# Speed of light in vacuum (m/s)
CLIGHT = 2.99792458e8

# Gravitational constant in SI units
G_SI = 6.6743E-11

# Speed of light (cm/s)
C_CGS = CLIGHT * 100.0e0

# Electric charge (C)
ECHARGE = 1.602176634e-19

M_TO_CM = 100.e0
MEGA = 1.e6

JOULE_TO_ERG = 1.e7

# 1.e-15 [m] x 100 [cm]
FM_TO_CM = 1.e-13

MEV_TO_ERG = MEGA * ECHARGE * JOULE_TO_ERG
MEV_FMCUBED_TO_ERG_CMCUBED = MEV_TO_ERG * (1.e0/FM_TO_CM)**3

# Solar mass in kg
msol_si = 1.9885E+30

c_si = 2.99792458E+8

# In m/cm
cm_to_m = 1e-2

# In kg/g
g_to_kg = 1e-3

# In kg/m^3
rho_nuc = 1.6605390666e17

# In m^-3
n_nuc = 0.1e45

# Baryonic mass in kg
m0 = rho_nuc/n_nuc
mev_si = 1.602176565E-13
m0_MeV = m0 * c_si * c_si/mev_si

# Conversion from MeV/fm^-3 to g/cm^3
eps_convert = mev_si * 1.e42 / (c_si*c_si)

# TO DO: convert into functions instead of using lambdas
si_to_geom = lambda gamma: (G_SI/(c_si*c_si))**(1.-gamma)/(c_si*c_si)
cgs_to_si = lambda gamma: (g_to_kg)**(1.-gamma)*(cm_to_m)**(3.*gamma-1.)
msol_geom = G_SI * msol_si/(c_si * c_si)

# Column header names used in the EoS Pandas data frames
TEMP_MEV = 'T/MeV'
NB_FM3 = 'nb/fm^-3'
YQ = 'Y_q'
PRESS_MEV_FM3 = 'P/MeV fm^-3'
PRESS_DYN_CM2 = 'P/dyn cm^-2'
PRESS_NB_MEV = 'P/nB MeV'
MU_LEPT_MEV = 'mu_l/MeV'
FREE_ENERGY_SCALED = 'F scaled'
INTER_ENERGY_SCALED = 'E scaled'
FREE_ENERGY_MEV = 'F/MeV'
ENTH_MEV = 'H/MeV'
ENTH_ERG = 'H/erg'
ENTH_C2 = 'H/c^2'
ENERGY_DENS_MEV_FM3 = 'epsilon/MeV fm^-3'
SOUND_SPEED_C2 = 'cs^2/c^2'
SCALED_MU_LEP = 'mu_l/mn'
MASS_ENERGY_DENS_G_CM3 = 'rho/g cm^-3'
MASS_ENERGY_DENS_G_GM3_LORENE = 'e/g cm^-3'
TEMP_INDEX = 'iT'
NB_INDEX = 'inB'
YQ_INDEX = 'iYq'
D2P_DHDY = 'd2p/dHdYe'
CHISQ = 'chi^2'
PROD_RATE = 'prod rate'

# Fitting coefficients for several 2D Equations of State
# that allow us to provide the value of kappa as a function
# of the charge fraction
rgsly4 = np.array(
    [-6.40094857e+07, 3.09187886e+08, -6.76599336e+08, 8.87244024e+08,
     -7.77161358e+08, 4.79859375e+08, -2.14840945e+08, 7.06741700e+07,
     -1.71262260e+07, 3.03735107e+06, -3.88012641e+05, 3.47171189e+04,
     -2.07328128e+03, 7.46564485e+01, -7.82041893e-01, -3.29368926e-02,
     9.91286215e-03])*140

sroapr = np.array(
    [-9.72248402e+05,  4.98386670e+06, -1.14644045e+07,  1.55841163e+07,
     -1.38535075e+07,  8.39016684e+06, -3.47082218e+06,  9.33807972e+05,
     -1.30439157e+05, -7.41043873e+03,  7.87189509e+03, -1.83259420e+03,
     2.43665247e+02, -2.13701068e+01,  1.68482751e+00, -7.47680070e-02,
     1.20158326e-02])*88.

hsdd2 = np.array(
    [-2.93804930e+07,  1.37030902e+08, -2.88883457e+08,  3.64099026e+08,
     -3.05796860e+08,  1.80589638e+08, -7.71225170e+07,  2.41267021e+07,
     -5.54029466e+06,  9.27037760e+05, -1.11080958e+05,  9.23734767e+03,
     -5.02270983e+02,  1.51366479e+01,  8.25865724e-02, -1.64532263e-02,
     3.07600302e-03])*300
