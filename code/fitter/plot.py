"""Module that contains plotting functions associated with the fit."""
import ast
import logging as log
import os
import sys

import matplotlib.pyplot as plt
import matplotlib
import numpy as np

from fitter import (eos, utils, consts)
from fitter.consts import MEGA, ECHARGE, JOULE_TO_ERG, FM_TO_CM, OUTPUTDIR

# Mapping between the data frame column name and
# the plot y-axis label.
VAR_TO_YAX_LABEL = {
    consts.PRESS_NB_MEV: r'$P/n_{B}$ MeV',
    consts.SCALED_MU_LEP: r'$\mu_{l}/ m_{n}$',
    consts.FREE_ENERGY_SCALED: r'$f/m_{n}n_{B} - 1$',
    consts.SOUND_SPEED_C2: r'$c_{s}^{2}/c^{2}$',
    consts.PRESS_DYN_CM2: r'$P$/dyn cm$^{-2}$',
    consts.MU_LEPT_MEV: r'$\mu_{l}/ MeV$',
    consts.D2P_DHDY: r'$|\partial^{2}p/\partial{H}\partial{Y_{e}}|$',
    consts.MASS_ENERGY_DENS_G_GM3_LORENE: r'$\rho$ g cm$^{-3}$',
    'Gamma': r'$\Gamma\equiv\frac{n_{B}}{P}\frac{\partial{P}}{\partial{n_{B}}}$'
}

# Mapping between the data frame column name and
# the suffix for the plot file names.
VAR_TO_SUFFIX = {
    consts.PRESS_NB_MEV: 'P_over_nB',
    consts.SCALED_MU_LEP: 'mu_l_over_mn',
    consts.FREE_ENERGY_SCALED: 'scaled_free_energy',
    consts.SOUND_SPEED_C2: 'spound_speed_squared',
    consts.PRESS_DYN_CM2: 'pressure_cgs',
    consts.MU_LEPT_MEV: 'mu_l',
    consts.D2P_DHDY: 'd2p_dHdYe',
    consts.MASS_ENERGY_DENS_G_GM3_LORENE: 'mass_energy_density',
    'Gamma': 'adiabatic_index'
}

FILE_FMT = '{eos}_{var}.png'
FILE_FMT_ERRORS = '{eos}_{var}_errors.png'

XLIM_MIN = 1.e-7
XLIM_MAX = 2.5

# Mapt the variable name to an appropriate set of
# limits on the y-axis
VAR_TO_YLIM = {
    consts.SOUND_SPEED_C2: (1.e-6, 2),
    consts.PRESS_NB_MEV: (None, None),
    consts.PRESS_DYN_CM2: (None, None),
    'Gamma': (0.9, 4.5),
    consts.D2P_DHDY: (None, None),
    consts.FREE_ENERGY_SCALED: (None, None)
    }


def adiabatic_index(table, Yq, tmin, lorene=True):
    """
    Calculate the adiabatic index by extracting the pressure (MeV/fm^3)
    and the baryon density (1/fm^3) from the relevant tables.

    """
    if lorene:
        press = utils.search_lorene(table, consts.PRESS_DYN_CM2, Yq)
        press = press / consts.MEV_FMCUBED_TO_ERG_CMCUBED
        nb = utils.search_lorene(table, consts.NB_FM3, Yq)
    else:
        press = table.search(consts.PRESS_MEV_FM3, tmin, Yq)
        nb = table.search(consts.NB_FM3, tmin, Yq)

    return eos.calc_gamma(press, nb)


def make_plots(plot_pars, eos_name, eos_2par, filetype='thermo'):
    """
    Compare several thermodynamic variables between the ComPOSE table
    and those generated from the Pearson fits, for the requested values
    of the charge fraction.

    :arg: dict plot_pars        : Plotting parameters as defined in the
                                  config. file
    :arg: string eos_name       : name of the equation of state
    :arg: bool eos_2par         : Is True for two-parameter EoSs
    :arg: string filetype       : Indicates the file to be considered

    """
    if eos_2par:
        var_list = {
            'thermo': [
                consts.PRESS_NB_MEV, consts.SCALED_MU_LEP,
                consts.FREE_ENERGY_SCALED
            ],
            'lorene': [
                consts.PRESS_DYN_CM2, consts.SOUND_SPEED_C2,
                consts.MU_LEPT_MEV, consts.D2P_DHDY,
                consts.MASS_ENERGY_DENS_G_GM3_LORENE, 'Gamma'
            ]
        }
    else:
        var_list = {
            'thermo': [consts.PRESS_NB_MEV,  consts.FREE_ENERGY_SCALED],
            'lorene': [consts.PRESS_DYN_CM2, consts.MASS_ENERGY_DENS_G_GM3_LORENE,
                       'Gamma']
        }

    # The eos.thermo table produced from the code is locted in the
    # current working directory.
    fpath = f'../{OUTPUTDIR}/{eos_name}'
    tables = eos.EoS(
        plot_pars['ref_table'], fpath)

    # The Lorene file created by the fitting scheme is written to the
    # scripts' working directory.
    lorene = lorene_ref = None

    # If the name of the Lorene file generated from the fitting is provided
    # then use that.
    if 'lorene_file' in list(plot_pars.keys()):
        lorene_file = plot_pars['lorene_file']
    else:
        lorene_file = f'{fpath}/eos_fit_{eos_name}.d'

    if filetype == 'lorene':
        lorene = utils.read_lorene_file(
            os.path.join(lorene_file), eos_2par)
        # Read in a "reference" Lorene EoS file if this has been specified
        # in the input configuration file.
        if 'ref_lorene' in list(plot_pars.keys()):
            lorene_ref = utils.read_lorene_file(
                plot_pars['ref_lorene'], eos_2par, old_col_format=False)

    yq_list = ast.literal_eval(plot_pars['yq_list'])

    # Over-ride the output directory if we are running in test mode
    figdir = plot_pars['figdir']
    figdir = figdir.replace('outputs', OUTPUTDIR)

    if not os.path.exists(figdir):
        os.makedirs(figdir)
    
    for var in var_list[filetype]:
        plot_fit(
            yq_list, var, tables, figdir, eos_name,
            lorene=lorene, lorene_ref=lorene_ref,
            plot_errors=plot_pars['plot_errors'])


def plot_fit(Yq_list, var, tables, outdir, eos_name, lorene=None,
             lorene_ref=None, find_nB_overlap=False, plot_errors=True):
    """
    Plot the specified thermodynamic variable as a function of the
    mass-energy density, for the ComPOSE table and those generated
    from the Pearson fits, for the requested values of the charge
    fraction. For those quantities contained in the Lorene file,
    a plot is also generated for a 'reference' Lorene file as a
    comparison.

    :arg: list Yq_list                : List of charge fractions to create
                                        plots for
    :arg: string var                  : Variable to consider
    :arg: eos.EoS tables              : Instance of the eos.EoS class for the
                                        EoS tables
    :arg: string outdir               : Output path for the plots
    :arg: string eos_name             : Name of the equation of state
    :arg: pandas.DataFrame lorene     : Data frame of the Lorene file created
                                        by the fitting procedure
    :arg: pandas.DataFrame lorene_ref : Idem, but for a "reference" file with
                                        which we want to compare
    :arg: find_nB_overlap             : If set to True, ensure that we plot against
                                        overlapping values of nB found in both the
                                        Lorene and CompOSE tables.

    """
    MAX_SUBPLOTS = 9
    N_NO_XLAB = 7

    font = {'size': 16}
    matplotlib.rc('font', **font)

    errors = {}
    nb_dict = {}
    num_yq = len(Yq_list)

    if num_yq > MAX_SUBPLOTS:
        sys.exit(f'A maximum of {MAX_SUBPLOTS} values for Yq is '
                 'currently supported.')

    # Get the lowest temperature entry (TO DO: this is also done
    # in eos.fitYq. Can this be done only in one place?
    tmin = tables.Tgrid[eos.iT]

    n = 1
    plt.figure(figsize=(30, 12))
    for iYq, Yq in enumerate(tables.Ygrid):

        nb_table = tables.search(consts.NB_FM3, tmin, Yq)

        # Read the variables calculated via the fitting scheme, either
        # from the eos.thermo file or from the Lorene format file.
        if lorene is None:
            # The thermo file uses a fortran indexing, so incremement the
            # temperature index by 1.
            var_from_fit = tables.search_thermo(var, eos.iT+1, iYq=iYq+1)
            nb_lorene = nb_table
        else:

            if var == 'Gamma':
                var_from_fit = adiabatic_index(lorene, Yq, tmin)
            else:
                var_from_fit = utils.search_lorene(lorene, var, Yq)

            nb_lorene = utils.search_lorene(
                lorene, consts.NB_FM3, Yq)
                
            if lorene_ref is not None:

                if var == 'Gamma':
                    var_from_ref_lorene = adiabatic_index(
                        lorene_ref, Yq, tmin)
                else:
                    var_from_ref_lorene = utils.search_lorene(
                        lorene_ref, var, Yq)

                nb_ref_lorene = utils.search_lorene(
                    lorene_ref, consts.NB_FM3, Yq)

        if var == consts.PRESS_DYN_CM2:
            var_from_table = tables.search(consts.PRESS_MEV_FM3, tmin, Yq)
            mev_to_erg = MEGA * ECHARGE * JOULE_TO_ERG
            var_from_table = var_from_table * mev_to_erg / FM_TO_CM**3
        elif var == consts.MASS_ENERGY_DENS_G_GM3_LORENE:
            var_from_table = tables.search(consts.ENERGY_DENS_MEV_FM3, tmin, Yq)
            var_from_table = var_from_table * (consts.MEV_TO_ERG / consts.C_CGS**2) * \
                (1.e0 / consts.FM_TO_CM**3)
        elif var == 'Gamma':
            var_from_table = adiabatic_index(tables, Yq, tmin, lorene=False)
        elif var != consts.D2P_DHDY:
            var_from_table = tables.search(var, tmin, Yq)

        if lorene is not None and var != consts.D2P_DHDY and find_nB_overlap:
            # Ensure that we use common values for the baryon number density
            # between the Lorene table and the ComPOSE table. A mismatch
            # may arise if we are using a version of Lorene which is
            # orthogonal in log-enthalpy and charge fraction, and so may give
            # number densitities that lie beyond the valid range for the
            # considered EoS.
            # TO-DO : remove 1.5 magic number and specify the max value via
            # TO-DO : an argument.
            condition = nb_lorene <= 1.5e0
            nb_lorene = nb_lorene[condition]
            var_from_fit = var_from_fit[condition]
            nb_table = nb_lorene

        if var != consts.D2P_DHDY:
            # d2p/dHdYe does not exist in the eos.table file so we can't
            # do an error comparison.
            nb_dict[f'{Yq:.5f}'] = nb_table
            errors[f'{Yq:.5f}'] = utils.calc_rel_error(
                var_from_table, var_from_fit)

        if len(Yq_list) > 1:
            # The value of Yq is in the list if any of the elements
            # give an error below 0.01. Needed for the two-parameter EoS
            inlist = np.min(np.abs(np.array(Yq_list) - Yq) / Yq_list) <= 0.01
        else:
            # One-parameter EoS
            inlist = Yq in Yq_list

        if inlist:
            log.info(f'Creating plot for Yq = {Yq}, var = {var}')

            if num_yq > 1:
                window = f'33{n}'
            else:
                window = f'11{n}'

            plt.subplot(int(window))

            if lorene_ref is not None:
                # For the derivative d2p/dHdY, there is no
                # equivalent quantity in eos.table. Also, consider
                # the absolute values, distinguishing between positive
                # and negative values via different symbols.
                if var == consts.D2P_DHDY:
                    # Ensure to values beyond the maximum allowed range
                    # for the baryon density
                    condition = nb_lorene <= 1.5e0
                    nb_lorene = nb_lorene[condition]
                    var_from_fit = var_from_fit[condition]

                    condition = nb_ref_lorene <= 1.5e0
                    nb_ref_lorene = nb_ref_lorene[condition]
                    var_from_ref_lorene = var_from_ref_lorene[condition]

                    pos_vals_fit = var_from_fit >= 0.e0
                    neg_vals_fit = var_from_fit < 0.e0

                    pos_vals_lorene = var_from_ref_lorene >= 0.e0
                    neg_vals_lorene = var_from_ref_lorene < 0.e0

                    l1, l2, l3, l4 = plt.plot(
                        nb_lorene[pos_vals_fit],
                        np.abs(var_from_fit[pos_vals_fit]), '*k',

                        nb_lorene[neg_vals_fit],
                        np.abs(var_from_fit[neg_vals_fit]), '_k',

                        nb_ref_lorene[pos_vals_lorene],
                        np.abs(var_from_ref_lorene[pos_vals_lorene]), '*r',

                        nb_ref_lorene[neg_vals_lorene],
                        np.abs(var_from_ref_lorene[neg_vals_lorene]), '_r',
                        linewidth=4, markersize=3.)
                else:
                    l1, l2, l3 = plt.plot(
                        nb_lorene, var_from_fit, '-k',
                        nb_table, var_from_table, ':y',
                        nb_ref_lorene, var_from_ref_lorene, '--r',
                        linewidth=4
                    )

            else:
                l1, l2 = plt.plot(
                    nb_lorene, var_from_fit, '-k',
                    nb_table, var_from_table, '--r',
                    linewidth=4
                )

            plt.xscale('log')
            if var not in [consts.SCALED_MU_LEP, consts.MU_LEPT_MEV]:
                plt.yscale('log')

            # Turn off x-axis ticks and lables for the first N_NO_XLAB
            # sub-label panel to prevent over-crowding.
            if n in range(1, N_NO_XLAB) and num_yq > 1:
                plt.xticks([])
                plt.xlabel('')
            else:
                plt.xlabel(r'$n_{B}$ / fm$^{-3}$')

            plt.ylabel(VAR_TO_YAX_LABEL[var])
            plt.xlim(XLIM_MIN, XLIM_MAX)

            if var in list( VAR_TO_YLIM.keys()):
                plt.ylim(VAR_TO_YLIM[var])

            plt.title(f'$Y_q$={Yq:.3f}')

            if lorene_ref is not None:
                plt.legend(['Pearson', 'CompOSE', 'Pseudo-polytrope'])
                if var == consts.D2P_DHDY:
                    plt.legend(
                        ['Pearson (> 0.0)', 'Pearson (< 0.0)',
                         'Pseudo-poly (> 0.0)', 'Pseudo-poly (< 0.0)']
                    )
            else:
                plt.legend(['Pearson', 'CompOSE'])

            plt.suptitle(eos_name.upper())

            n += 1

    fname = FILE_FMT.format(eos=eos_name, var=VAR_TO_SUFFIX[var])
    figpath = os.path.join(outdir, fname)
    log.info(f'Writing out {figpath}.')
    plt.savefig(figpath)

    # 2D errors not stored for d2p/dHdYe.
    if var != consts.D2P_DHDY and plot_errors:
        fit_errors(errors, nb_dict, outdir, eos_name, var)


def fit_errors(errors, nb, outdir, eos_name, var,
               vmin=-20, vmax=20):
    """
    Plot fitting errors as 2D surface plots as a function of neutron
    star mass-energy density for the specified thermodynamic variable,
    for each slice of the charge fraction, Ye.

    :arg: dict errors      : Fitting errors for each slice along Yq
    :arg: dict nb          : Baryon densities for each slice along Yq
    :arg: string outdir    : Path to write figures to
    :arg: string eos_name  : Name of the equation of state
    :arg: string var       : Variable to plot
    :arg: float vmin       : Min. error value to plot
    :arg: float vmax       : Max. error value to plot

    """
    STEP = 2
    # Provide the labelling on the y-axis for the slices. Do not provide
    # labels for every slice to avoid over-crowding.
    ylabels = {}
    for i, key in enumerate(list(errors.keys())):
        if i % 2 == 0:
            ylabels[key] = key
        else:
            ylabels[key] = ''

    yq_slices = list(errors.keys())
    nslices = len(yq_slices)
    log.info(f'There are {nslices} slices along Yq for variable {var}.')

    plt.figure(figsize=(15, 8))
    for i in range(nslices):
        islice = yq_slices[i]

        # Create an array holding the errors for the
        # current slice.
        x = nb[islice]
        y = np.arange(STEP*i, STEP*(i+1), 1)
        z = np.zeros([len(y), len(x)])

        z[:, :] = errors[islice][0:]

        plt.pcolormesh(x, y, z, cmap='coolwarm', shading='gouraud',
                       vmin=vmin, vmax=vmax)

        plt.xscale('log')
        plt.xlim(XLIM_MIN, XLIM_MAX)

        plt.text(0.03, y[1]-0.5, ylabels[islice], fontsize=12, color='k')

    cbar = plt.colorbar(extend='both')
    cbar.set_label('$100x(ΔP/P)$')
    plt.yticks([])
    plt.xlabel(r'$n_{B}$/ $fm^{-3}$')
    plt.xlim(XLIM_MIN, XLIM_MAX)
    plt.suptitle(
        'Fit relative errors for %s and EoS %s' % (
            VAR_TO_YAX_LABEL[var], eos_name)
    )

    fname = FILE_FMT_ERRORS.format(eos=eos_name, var=VAR_TO_SUFFIX[var])
    figpath = os.path.join(outdir, fname)
    log.info(f'Writing out {figpath}.')
    plt.savefig(figpath)


def plot_two_par_fit(logrho, Ygrid, logP, best_fit, pars,
                     Yqlist=[0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]):
    """
    Compare the tabulated and best fit values for the pressure
    for a two-parameter EoS.

    """
    logPbest = np.log10(best_fit(pars, np.power(10, logrho), Ygrid))
    
    logP = logP.reshape(len(Ygrid), len(logrho))
    logPbest = logPbest.reshape(len(Ygrid), len(logrho))

    n = 1
    plt.figure(figsize=(25, 25))
    for iY, Yq in enumerate(Ygrid):
        if Yq in Yqlist:
            plt.subplot(3, 3, n)
            plt.plot(
                np.power(10, logrho), np.power(10, logP[iY, :]), '-k',
                np.power(10, logrho), np.power(10, logPbest[iY, :]), '--r',
                linewidth=4)
            plt.xscale('log')
            plt.yscale('log')
            plt.xlabel(r'log($\rho$ g/cm$^{3}$)')
            plt.ylabel(r'log(p/MeV fm$^{-3}$)')
            plt.title(f'Y_q={Yq:.5f}')
            n += 1

    plt.show()

    err = utils.calc_rel_error(np.power(10, logP), np.power(10, logPbest))
    plt.pcolormesh(logrho, Ygrid, err, cmap='PiYG')
    plt.xlabel(r'log($\rho$ g/cm$^{3}$)')
    plt.ylabel(r'log(p/MeV fm$^{-3}$)')
    plt.colorbar()
    plt.show()


class TableComparitor:
    """
    Create an object that will allow us to graphically compare
    a Lorene table generated by the fits with a ComPOSE table.

    """
    def __init__(self, table_one, table_two, Yselect, Ygrid_file):
        self.table_one = table_one
        self.table_two = table_two
        self.Yselect = Yselect
        self.Ygrid_file = Ygrid_file

    @staticmethod
    def _read(table, var, iY, Yq):
        shape = utils.lorene_grid_size(table)
        if table.endswith('.d'):
            df = utils.read_lorene_file(table, eos_2par=True)
            arr = np.reshape(df[var].to_numpy(), (shape[1], shape[0]))
            nB = np.reshape(df[consts.NB_FM3].to_numpy(), (shape[1], shape[0]))
            return nB[iY], arr[iY]
        elif 'eos.table' in table:
            thermo_path = os.path.basename(table)
            compose_table = eos.EoS(table, thermo_path)
            arr = compose_table.search(var, 0.1, Yq).to_numpy()
            nB = compose_table.search(consts.NB_FM3, 0.1, Yq).to_numpy()
            return nB, arr

    @property
    def Ygrid(self):
        grid = np.loadtxt(self.Ygrid_file, skiprows=2)
        return grid

    def compare(self, var, xlims=None, ylims=None, xlog='linear',
                ylog='linear'):

        isubplot = 0
        plt.figure(figsize=(20, 20))
        for iY, Yq in enumerate(self.Ygrid):
            if round(Yq, 2) in self.Yselect:
                isubplot += 1

                nB1, arr1 = self._read(self.table_one, var, iY, Yq)
                nB2, arr2 = self._read(self.table_two, var, iY, Yq)

                plt.subplot(3, 3, isubplot)
                plt.plot(nB1, arr1, '+')
                plt.plot(nB2, arr2, 'o')

                if xlims is not None:
                    plt.xlim(xlims)
                if ylims is not None:
                    plt.ylim(ylims)

                plt.xscale(xlog)
                plt.yscale(ylog)
                plt.title(f'Y = {Yq}')

                plt.xlabel(r'$n_{B}$ [$fm^{-3}$]')
                plt.ylabel(VAR_TO_YAX_LABEL[var])

                plt.grid()

        plt.show()
        
