"""
Module that contains functions and classes for handling and
fitting the ComPOSE Equations of State tables.

"""
import itertools
import logging as log
import os

import numpy as np
import pandas as pd

from fitter import (thermo, utils, consts, pearson)

# Specify the grid index corresponding to the lowest temperature, since
# we are currently only considering cold NS matter.
iT = 0
TEST_MODE = os.getenv('TEST_MODE')


def calc_gamma(pressure, nB):
    """
    Calculate the adiabatic index.

    :arg: np.array pressure : Pressure in MeV/fm^3
    :arg: np.array nB       : Baryon density 1/fm^3

    """
    return (nB/pressure) * np.gradient(
        pressure, nB, edge_order=2)


def get_grid_values(column):
    """
    Get a unique list of values in a column that define the points
    in the grid for the quantity associated with that column.

    """
    grid_values = sorted(list(set(column)))
    return np.array(grid_values, dtype=np.float64)


class EoS:

    """Class to handle the reading in of the compOSE EoS tables."""
    def __init__(self, table_path, eos_path):
        """
        :arg: string table_path : Path to the eos.table file
        :arg: string eos_path   : Path containing the eos and grid info.

        """
        self.table_path = table_path
        self.eos_path = eos_path
        self.thermo_path = os.path.join(self.eos_path, 'eos.thermo')
        self.particles = self.read_particle_info()
        self.mass_neutron = self.particles['mn'].to_numpy()
        self.table = self.read_table()
        self.thermo = self.read_eos_thermo()
        self.has_two_param = len(self.Ygrid) > 1

    def read_table(self):
        """Read in the EoS table and add appropriate column headers."""
        # TO-DO. The order and names of the columns can be automatically
        # determined by the eos.info.json file. We should also add checks
        # that the required fields are there.
        columns = [
            consts.TEMP_MEV, consts.NB_FM3, consts.YQ,
            consts.PRESS_MEV_FM3, consts.MU_LEPT_MEV,
            consts.FREE_ENERGY_SCALED, consts.FREE_ENERGY_MEV,
            consts.ENTH_MEV, consts.ENERGY_DENS_MEV_FM3,
            consts.SOUND_SPEED_C2
        ]

        df = pd.read_csv(self.table_path, delimiter=r'\s+', header=None,
                         names=columns, dtype=np.float64)

        mev_to_g = consts.MEV_TO_ERG / consts.C_CGS**2
        df[consts.MASS_ENERGY_DENS_G_CM3] = df[consts.ENERGY_DENS_MEV_FM3] * \
            mev_to_g * (1.e0/consts.FM_TO_CM)**3

        df[consts.ENTH_ERG] = df[consts.ENTH_MEV] * consts.MEV_TO_ERG
        df[consts.PRESS_NB_MEV] = df[consts.PRESS_MEV_FM3] / df[consts.NB_FM3]
        df[consts.SCALED_MU_LEP] = df[consts.MU_LEPT_MEV] / \
            self.mass_neutron

        return df

    def read_particle_info(self):
        """Read the particle masses from the eos.thermo file."""
        particles = pd.read_csv(
            self.thermo_path, delimiter=r'\s+', nrows=1, header=None)
        particles.columns = ['mn', 'mp', 'lp']

        return particles

    def read_eos_thermo(self):
        """Read the thermo quantities from the eos.thermo file."""
        df = pd.read_csv(
            self.thermo_path, delimiter=r'\s+', skiprows=1, header=None,
            usecols=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        )

        df.columns = [
            consts.TEMP_INDEX, consts.NB_INDEX, consts.YQ_INDEX,
            consts.PRESS_NB_MEV, 'q2', 'q3', 'q4',
            consts.SCALED_MU_LEP, consts.FREE_ENERGY_SCALED,
            consts.INTER_ENERGY_SCALED, 'opt_fields_flag'
        ]

        return df

    def search(self, var, temp, Yq=None, nb=None):
        """
        Search the eos.table data frame for values given in a
        particular column, according to the temperature, electron
        fraction and, optionally, the Baryon number density.

        :arg: string var          : Column heading name associated with
                                    the desired variable
        :arg: float temp          : Temperature value used for the search
        :arg: float Yq            : Electron fraction value used for the search
        :arg: float nb            : Baryon number density value used for
                                    the search (optional)

        """
        condition = (self.table[consts.TEMP_MEV] == temp)

        # TO-DO: This is not the best way when handling floats.
        if Yq is not None:
            condition = condition & (self.table[consts.YQ] == Yq)

        if nb is not None:
            condition = condition & (self.table[consts.NB_FM3] == nb)

        return self.table[var][condition].to_numpy()

    def search_thermo(self, var, iT, inB=None, iYq=None):
        """
        Search the eos.thermo data frame for values given in a
        particular column, according to indices for the temperature
        and optionally for the baryon number density and the charge
        fraction.

        :arg: string var   : Column heading name associated with the
                             desired variable
        :arg: int iT       : Temperature index used for the search
        :arg: int inB      : Baryon num. density index used for the search
        :arg: int iYq      : Charge fraction index used for the search.

        """
        condition = (self.thermo[consts.TEMP_INDEX] == iT)
        if inB is not None:
            condition = condition & (self.thermo[consts.NB_INDEX] == inB)
        if iYq is not None:
            condition = condition & (self.thermo[consts.YQ_INDEX] == iYq)

        return self.thermo[var][condition].to_numpy()

    @property
    def Tgrid(self):

        """Get the table's grid values for temperature in MeV"""
        return get_grid_values(self.table[consts.TEMP_MEV])

    @property
    def Ygrid(self):
        """
        Get the table's grid values for the charge fraction
        (dimensionless)

        """
        return get_grid_values(self.table[consts.YQ])

    @property
    def NBgrid(self):
        """
        Get the table's grid values for the Baryon number density (fm^-3).

        """
        return get_grid_values(self.table[consts.NB_FM3])

    def remove_pions(self, tmin):
        """
        Remove the phase transition associated with the appearance
        of pions in the core. Tmin is the lowest temperature for the
        considered equation of state (MeV).

        """
        Pupdated = []
        for iY, Yq in enumerate(self.Ygrid):
            Ptab = self.search(
                consts.PRESS_MEV_FM3, tmin, Yq)
            rhotab = self.search(
                consts.MASS_ENERGY_DENS_G_CM3, tmin, Yq)

            # TO DO: remove hardwired values of thomin and make an arg
            Pupdated.append(utils.remove_pions(Ptab, rhotab, 2.5e14))

        self.table[consts.PRESS_MEV_FM3] = np.array(Pupdated).T.flatten()


class EoSFit:
    """Class describing a fit for a CompOSE tabulated equation of state"""
    def __init__(self, fit_config, table_name):
        """
        Initialise the class

        :arg: configparser fit_config : configparser object containing the
                                        parameters needed for the fit.
        :arg: str table_name          : Name of CompOSE interpolated table
                                        to create fits for

        """
        self.fit_config = fit_config
        self.table_name = table_name

        self.eos_path = fit_config.get('eos', 'table_path')
        self.xtol = fit_config.getfloat('fitting', 'tolerance')
        self.fit_method = fit_config.get('fitting', 'method')
        self.nb_nsize = fit_config.getint('nb_model_grid', 'nsize')
        self.nb_start = fit_config.getfloat('nb_model_grid', 'start')
        self.nb_end = fit_config.getfloat('nb_model_grid', 'end')

        self.eos_table = os.path.join(self.eos_path, self.table_name)
        self.eos = EoS(self.eos_table, self.eos_path)
        self.table = self.eos.table
        self.eos_name = fit_config.get('eos', 'name')

        self.nBlim1 = fit_config.getfloat(
            'fitting', 'nBlim1')
        self.nBlim2 = fit_config.getfloat(
            'fitting', 'nBlim2')
        self.kappa = self.get_kappa()
        self.gamma = fit_config.getfloat(
            'fitting', 'gamma')

        # TO DO: I don't think this is used.
        self.nb_grid_file = fit_config.get(
            'nb_model_grid', 'grid_file', fallback=None)

        self.mintol = fit_config.getfloat(
            'fitting', 'mintol', fallback=None)
        self.minmethod = fit_config.get(
            'fitting', 'minmethod', fallback=None)

    def get_kappa(self):
        """
        Get the value of kappa for the crust. The value of kappa can be a
        string (corresponding to a name of a two-parameter EoS) if we
        want to specify the fitting coefficients needed to calculate
        kappa as a function of charge fraction for that EoS. Fitting
        coefficients are stored in fitter.consts.

        """
        try:
            kappa = self.fit_config.getfloat('fitting', 'kappa')
        except ValueError:
            kappa = self.fit_config.get(
                'fitting', 'kappa')

        return kappa

    @property
    def new_Nb(self):
        """
        Define the new grid for the Baryon number density onto which we
        should create the required thermodynamic variables.

        """
        # TO DO: Was used for testing purposes. Remove?
        if not self.nb_grid_file:
            new_Nb = np.linspace(
                np.log10(self.nb_start), np.log10(self.nb_end), self.nb_nsize,
                dtype=np.float64
            )
            return np.power(10, new_Nb, dtype=np.float64)
        else:
            data = np.loadtxt(self.nb_grid_file, skiprows=9)
            new_Nb = data[:, 1]
            return new_Nb

    def write_thermo(self, neutron_star):
        """
        Write out the thermo diagnostics in a format similar to
        the CompOSE eos.thermo file.

        :args: thermo.FitTable neutron_star : Object containing
                                              thermo quantities
                                              calculated from a fit

        """
        # TO-DO Update neutron_star object name to "thermo_quantities?"
        thermo_file = 'eos.thermo'
        fmt = [
            '%6i', '%5i', '%5i', '%24.16E', '%24.16E', '%24.16E', '%24.16E',
            '%24.16E', '%24.16E', '%24.16E', '%5i'
        ]

        scaled_mul = \
            neutron_star.lepton_chemical_potential / neutron_star.Mn
        scaled_pressure = \
            (neutron_star.pressure/consts.MEV_FMCUBED_TO_ERG_CMCUBED) / \
            self.new_Nb

        particles = self.eos.read_particle_info()

        output = [
            scaled_pressure,
            scaled_mul,
            neutron_star.scaled_free_energy,
            neutron_star.scaled_internal_energy
        ]

        particle_info = np.column_stack(
            [particles['mn'][0], particles['mp'][0], particles['lp'][0]]
            )

        # Write the header information specifying the particle masses
        fpath = os.path.join(f'{consts.OUTPUTDIR}/{self.eos_name}/')
        fname = os.path.join(f'../{fpath}/{thermo_file}')
        with open(fname, 'w') as handle:
            np.savetxt(handle, particle_info, fmt=[
                '  %.14f', '  %.6f', '     %i'])

        # Output for each slice along Yq
        for iYq, slices in enumerate(zip(*output)):
            Nindices = np.array(range(0, self.nb_nsize)) + 1
            Yindices = np.array([iYq] * self.nb_nsize) + 1
            Tindices = np.array([0] * self.nb_nsize) + 1

            # Even though they are not needed, we still need to add
            # columns associated with Q2, Q3 and Q4, and a flag at the
            # end of each row to indicate that additional fields are
            # not used. Set the column values to zero.
            dummy_float = np.array([0.e0] * self.nb_nsize)
            dummy_int = np.array([0] * self.nb_nsize)
            data = np.column_stack(
                [
                    Tindices, Nindices, Yindices,
                    slices[0], dummy_float, dummy_float,
                    dummy_float, slices[1], slices[2], slices[3],
                    dummy_int
                ]
            )

            with open(fname, 'a') as handle:
                np.savetxt(handle, data, fmt)

    def write_grid(self, grid, fname):
        """
        Write the output file containing the grid information.

        :arg: np.array grid : Grid data to output
        :arg: str fname     : Name of output file containing grid info

        """
        # Output the grid information to 7 decimal places, to be consistent
        # with the CompOSE software.
        fpath = os.path.join(f'../{consts.OUTPUTDIR}/{self.eos_name}')
        if not os.path.exists(fpath):
            os.makedirs(fpath)

        fout = os.path.join(f'{fpath}/{fname}')
        with open(fout, 'w+') as file_handle:
            np.savetxt(file_handle, np.array([1]), fmt='    %i')
            np.savetxt(file_handle, np.array([len(grid)]), fmt='  %i')
            np.savetxt(
                file_handle, grid, fmt='  %.16E'
                )

    def write_lorene_one_param(self, neutron_star):
        """
        Write a Lorene input file that contains thermodynamic quantities
        needed to calculate the structure of a neutron star.

        :args: thermo.FitTable neutron_star : Object containing
                                              thermo quantities
                                              calculated from a fit

        """
        fname = f'eos_fit_{self.eos_name}.d'
        fmt = [
            '%6i', '%24.16E', '%24.16E', '%24.16E'
            ]

        columns = (
            '{0}{1:>30}{2:>28}'.format(
                'n_B [1/fm^3]', 'e [g/cm^3]', 'P [dyn/cm^3]')
            )

        # The branch and versions will change, so set these to an
        # empty string if we are running automated testing. This will
        # avoid test failures when comparing to KGO.
        git_version = (
            '\n' if TEST_MODE is not None
            else utils.git_latest_version()
        )
        git_branch = (
            '\n' if TEST_MODE is not None
            else utils.git_branch_name()
        )

        header = (
            f'# {self.eos_name} \n'
            '#\n'
            '# EoS created from fit of 1 parameter EoS. Parameter is n_B \n'
            f'# {git_branch}'
            f'# {git_version}'
            f'{len(self.new_Nb)} in n_B\n'
            '#\n'
            f'#       {columns}\n'
            '#\n'
            )

        fpath = os.path.join(f'../{consts.OUTPUTDIR}/{self.eos_name}/')
        if not os.path.exists(fpath):
            os.makedirs(fpath)

        fout = os.path.join(f'{fpath}/{fname}')

        with open(fout, 'w') as handle:
            handle.write(header)

        Nindices = np.array(range(0, self.nb_nsize))
        data = np.column_stack(
            [Nindices, self.new_Nb, neutron_star.mass_energy_density[0],
             neutron_star.pressure[0]]
            )

        with open(fout, 'a') as handle:
            np.savetxt(handle, data, fmt)

    def write_lorene_two_param(self, neutron_star):
        """
        Write Lorene input file that contains thermodynamical quantities
        needed to run hydrodynamical simulations of oscillating NSs with
        a two-parameter EoS.

        """
        fname = f'eos_fit_{self.eos_name}.d'
        fmt = [
            '%6i', '%24.16E', '%24.16E', '%24.16E', '%24.16E',
            '%24.16E', '%24.16E', '%24.16E', '%24.16E', '%24.16E'
            ]

        columns = (
            '{0}{1:>30}{2:>28}{3:>18}{4:>30}{5:>25}{6:>25}{7:>33}'
            '{8:>25}'.format(
                'n_B [1/fm^3]', 'e [g/cm^3]', 'P [dyn/cm^3]', 'H [c^2]',
                'Y_e [dimensionless]', 'c_s^2 [c^2]', 'mu_e [MeV]',
                'd2p/dHdYe [dyn c^-2 cm^-2]', 'Prod rate [s^-1]')
            )

        # The branch and versions will change, so set these to an
        # empty string if we are running automated testing. This will
        # avoid test failures when comparing to KGO.
        git_version = (
            '\n' if TEST_MODE is not None
            else utils.git_latest_version()
        )
        git_branch = (
            '\n' if TEST_MODE is not None
            else utils.git_branch_name()

        )

        header = (
            f'# {self.eos_name} \n'
            '#\n'
            '# EoS created from fit of 2 parameter EoS. Parameters are n_B '
            'and Y_e\n'
            f'# {git_branch}'
            f'# {git_version}'
            f'{len(self.new_Nb)}    {len(self.eos.Ygrid)} in n_B and Y_e\n'
            '#\n'
            f'#       {columns}\n'
            '#\n'
            )

        fpath = os.path.join(f'../{consts.OUTPUTDIR}/{self.eos_name}/')
        if not os.path.exists(fpath):
            os.makedirs(fpath)

        fout = os.path.join(f'{fpath}/{fname}')

        with open(fout, 'w') as handle:
            handle.write(header)

        output = [
            neutron_star.mass_energy_density,
            neutron_star.pressure,
            neutron_star.log_enthalpy,
            neutron_star.square_of_sound_speed,
            neutron_star.lepton_chemical_potential,
            neutron_star.d2p_dlogHdYq,
            neutron_star.neutrino_prod_rate,
        ]

        for iYq, slices in enumerate(zip(*output)):
            Nindices = np.array(range(0, self.nb_nsize))
            Yqs = np.array([self.eos.Ygrid[iYq]] * self.nb_nsize,
                           dtype=np.float64)

            data = np.column_stack(
                [Nindices, self.new_Nb, slices[0],
                 slices[1], slices[2], Yqs,
                 slices[3], slices[4],
                 slices[5], slices[6]]
                )

            with open(fout, 'a') as handle:
                np.savetxt(handle, data, fmt)

    def fit(self, iT, write_diags=True):
        """
        Perform a fit using the Pearson formalism, and attach a polytropic
        crust, for a one- or two-parameter equation of state.

        """
        # Grab a sub-set of the BSk24 fitting parameters, corresponding
        # just to the core region, in order to supply initial guesses for
        # the fit parameters.
        guess_fit_params = [
                pearson.pearson_params[5:17], pearson.pearson_params[20:]]
        guess_fit_params = list(
                itertools.chain.from_iterable(guess_fit_params))

        # Get the lowest temperature in the table (corresponding
        # to the first grid index, iT.
        tmin = self.eos.Tgrid[iT]

        table = thermo.FitTable(
            self.eos_name, self.eos, pearson.model_core,
            self.kappa, self.gamma, self.nBlim1, self.nBlim2,
            tmin, self.new_Nb, guess_fit_params, self.xtol,
            self.fit_method, self.mintol, self.minmethod)

        table.make()
        table.write_fit_pars()

        is_one_par = len(self.eos.Ygrid) == 1
        if write_diags:
            # This isn't really needed for the hydro simulations
            # but it is useful for diagnostic purposes.
            log.info('Writing out eos.thermo.')
            self.write_thermo(table)

            log.info('Writing Lorene file.')
            if is_one_par:
                self.write_lorene_one_param(table)
            else:
                self.write_lorene_two_param(table)
                log.info('Writing out grid information.')
                self.write_grid(self.new_Nb, 'eos.nb')
                self.write_grid(self.eos.Ygrid, 'eos.yq')
                self.write_grid(self.eos.Tgrid, 'eos.t')
