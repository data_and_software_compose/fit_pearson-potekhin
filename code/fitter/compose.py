"""
Module associated with interacting with the CompOSE database and the
associated software.

"""
import os
import glob
import shutil
import subprocess
import logging as log

from urllib.request import urlretrieve

import datalad.api


def run_compose(executable, *inputs):
    """
    Run the CompOSE executable and automatically supply the inputs to
    the command-line prompts.

    :arg: str executable : Name of the executable to run
    :arg: str inputs     : Responses to supply to the program

    """
    input_args = '\n'.join(inputs)

    proc = subprocess.Popen(
        [executable],
        stdout=subprocess.PIPE, stdin=subprocess.PIPE,
        stderr=subprocess.PIPE, encoding='utf-8')

    stdout, stderr = proc.communicate(input_args)
    return stdout, stderr


class CompOSE:
    def __init__(self, table_url, table_path, code_path):
        """
        :arg: str table_url       : URL to the zip file containing tables
        :arg: str table_path      : Path where zip file should be downloaded
                                    created by the CompOSE code
        :arg: str code_path       : Path to the CompOSE software

        """
        self.table_url = table_url
        self.table_path = table_path
        self.code_path = code_path

    def download_data(self):
        """Download and unzip tables into the specified directory"""

        log.info(f'Downloading from {self.table_url}')
        dest = os.path.join(self.table_path, 'eos.zip')
        source = os.path.join(self.table_url, 'eos.zip')

        if not os.path.exists(dest):
            path, headers = urlretrieve(source, dest)
        else:
            log.info(f'File {dest} already exists.')

        if not os.path.exists(os.path.join(self.table_path, 'eos.nb')):
            log.info(f'Unzipping {dest}')
            subprocess.run(
                ['unzip', '-d', self.table_path, '-o', dest], check=True)
        else:
            log.info(f'File {dest} already extracted.')

    def make_tables(self, file_quantities, file_parameters, table_name):
        """Create an interpolated table based on pre-existing
        eos.quantities and eos.parameters files. An interpolated
        table is thus created from the CompOSE software.

        """
        # Change cwd to the location of the compose software
        cwd = os.getcwd()
        os.chdir(self.code_path)

        # We use datalad to store the CompOSE code as a sub-directory
        # via git annex. We need to extract it if not already in place.
        datalad.api.get('.', dataset='../../')

        subprocess.run('make')
        os.chdir(cwd)

        # Copy the executable, the 'quantity' and the 'parameters' file
        # to the same location as the downloaded tables
        files = [
            os.path.join(self.code_path, 'compose'),
            file_quantities,
            file_parameters
        ]

        for infile in files:
            log.info(f'Copying {infile} to {self.table_path}')
            shutil.copy(infile, self.table_path)

        os.chdir(self.table_path)

        if not os.path.exists(table_name):
            log.info('Creating tables.')
            run_compose('./compose', '3')

            log.info(f'Moving eos.table to {table_name}')
            shutil.move('eos.table', table_name)
        else:
            log.info(f'Table {table_name} already exists.')

        os.chdir(cwd)

    def housekeep(self):
        """Remove the EoS tables and clean the CompOSE code once any processing
        on the data them has completed."""

        cwd = os.getcwd()

        data = glob.glob(os.path.join(self.table_path, 'eos.*'))
        readme = glob.glob(os.path.join(self.table_path, 'README*'))
        compose = glob.glob(os.path.join(self.table_path, 'compose'))
        data.extend(readme)
        data.extend(compose)

        for _file in data:
            log.info(f'Deleting {_file}')
            os.remove(_file)

        os.chdir(self.code_path)
        subprocess.run(['make', 'clean'])

        os.chdir(cwd)
