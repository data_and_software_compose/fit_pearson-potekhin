"""
Module associated with the analytical representation described by
Haensel & Potekhin 2004, A&A, 428, 191-197 and Pearson et al. 2018,
MNRAS, 481, 2994-3026

"""
import logging as log

from functools import lru_cache

import numpy as np
import scipy.optimize
import scipy.interpolate
import sympy.utilities.lambdify

from fitter import (utils, consts)

# Pearson et al. 2018 fit parameters, corresponding to p1 to p23 in
# Appendix C.
pearson_params = [
    6.795e0, 5.552e0, 0.00435e0, 0.13963e0, 3.636e0, 11.943e0, 13.848e0,
    1.3031e0, 3.644e0, -30.840e0, 2.2322e0, 4.65e0, 14.290e0, 30.08e0,
    -2.080e0, 1.1e0, 14.71e0, 0.099e0, 11.66e0, 5.00e0, -0.095e0, 14.15e0,
    9.1e0
    ]

# Create sympy symbols so that we can easily calculate the derivative
# of the Pearson fit function.
r, p1, p2, p3, p4, p5, p6, p7 = sympy.symbols(
    'r p1 p2 p3 p4 p5 p6 p7')
p8, p9, p10, p11, p12, p13, p14, p15 = sympy.symbols(
    'p8 p9 p10 p11 p12 p13 p14 p15')
p16, p17, p18, p19, p20, p21, p22, p23 = sympy.symbols(
    'p16 p17 p18 p19 p20 p21 p22 23')


# Define core mathematical functions that are used within
# the Pearson/ Potekhin fitting model. The sympy argument
# allows us to express the functions using sympy symbols.
def inverse_exp(x, y, z, _sympy=False):
    """Define a Fermi-like function."""
    if _sympy:
        return (sympy.exp(x * (y - z)) + 1.e0)**-1.e0
    else:
        return (np.exp(x * (y - z)) + 1.e0)**-1.e0


def linear(x, y, z):
    """Define a linear function"""
    return x + (y * z)


def inverse_quad(w, x, y, z):
    """Define an inverse quadratic-like function."""
    return w / (1.e0 + (x * (y - z))**2.e0)


def model(r, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13,
          p14, p15, p16, p17, p18, p19, p20, p21, p22, p23,
          _sympy=False, cgs=False):
    """
    Define the Potekhin-Pearson fitting model that covers the whole
    NS domain.

    :arg: numpy.array r  : log10 of the mass-energy density (g/cm^-3)
    :arg: float p1..p23  : Fit coefficients
    :arg: bool _sympy    : Set to true to define function using sympy
    :arg: bool cgs       : Set to True to output pressure in cgs units

    """
    if cgs:
        K = 0.0e0
    else:
        K = np.log10(1.e0 / consts.MEV_FMCUBED_TO_ERG_CMCUBED)

    # This factor contains additional terms associated with the
    # outer crust
    k1 = (
        p1 + p2 * r + p3 * r**3
    ) / (1 + p4 * r)

    return (
        K + k1 * inverse_exp(p5, r, p6, _sympy) +
        linear(p7, p8, r) * inverse_exp(p9, p6, r, _sympy) +
        linear(p10, p11, r) * inverse_exp(p12, p13, r, _sympy) +
        linear(p14, p15, r) * inverse_exp(p16, p17, r, _sympy) +
        inverse_quad(p18, p20, r, p19) + inverse_quad(p21, p23, r, p22)
    )


def model_core(r, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16,
               p17, p21, p22, p23, _sympy=False, cgs=False):
    """
    Define the fitting model but just for the inner/ outer core region

    :arg: numpy.array r  : log10 of the EoS density (g/cm^3)
    :arg: float p6...p23 : Pearson fit parameters
    :arg: bool _sympy    : Set to true to express in terms of sympy
                           symbols
    :arg: bool cgs       : Set to True to express pressure in cgs units

    """
    if cgs:
        K = 0.0e0
    else:
        K = np.log10(1.e0 / consts.MEV_FMCUBED_TO_ERG_CMCUBED)

    return (
        K + linear(p7, p8, r) * inverse_exp(p9, p6, r, _sympy) +
        linear(p10, p11, r) * inverse_exp(p12, p13, r, _sympy) +
        linear(p14, p15, r) * inverse_exp(p16, p17, r, _sympy) +
        inverse_quad(p21, p23, r, p22)
    )


@lru_cache
def model_core_deriv():
    """
    Uses sympy to calculate the derivative of the Pearson fit
    function only including the terms associated with the core,
    and then convert the result into a function that can be used
    in subsequent calculations.

    """
    fitexpr = model_core(
        r, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16,
        p17, p21, p22, p23, _sympy=True
    )

    return sympy.utilities.lambdify(
        [r, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16,
         p17, p21, p22, p23], sympy.diff(fitexpr, r), 'numpy'
    )


# TO-DO: Ygrid no longer used and should be removed
class PearsonModel:
    """Class that defines a fit model using the Pearson-Potekhin scheme"""
    def __init__(self, log10rho, log10P, Ygrid,
                 xtol=1.e-3, fit_method='trf'):
        """
        Initialise the class

        :arg: numpy.array log10rho  : log10 of mass-energy density (g cm^-3)
        :arg: numpy.array pressure  : log10 of pressure (MeV fm^-3)
        :arg: numpy.array Ygrid     : Grid values for electronic
                                      charge fraction
        :arg: float xtol            : Fitting tolerance
        :arg: string fit_method     : Fitting method

        """
        self.log10rho = log10rho
        self.log10P = log10P
        self.Ygrid = Ygrid
        self.xtol = xtol
        self.fit_method = fit_method
        self.deriv_pearson = model_core_deriv()

    def __str__(self):
        info = (f'Tolerance: {self.xtol}\n'
                f'Fit method: {self.fit_method}\n'
                f'Grid size in density: {self.log10rho.shape}\n'
                f'Grid size in pressure: {self.log10P.shape}\n'
                )

        return info

    def model_deriv(self):
        """
        Uses sympy to calculate the derivative of the Pearson fit
        function, and then convert the result into a function that can be
        used in subsequent calculations.

        """
        fitexpr = model(
            r, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11,
            p12, p13, p14, p15, p16, p17, p18, p19, p20, p21,
            p22, p23, _sympy=True
        )

        return sympy.utilities.lambdify(
            [r, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13,
             p14, p15, p16, p17, p18, p19, p20, p21, p22, p23],
            sympy.diff(fitexpr, r), 'numpy'
        )

    # TO-DO: make more flexible to use derivative either just for core or for
    # whole NS using full analytical expression.
    def _deriv_pearson(self, popt, density):
        """A wrapper function around the deriv_pearson function,
        that allows us to pass in an array for the optimal fitting
        parameters.

        """
        return self.deriv_pearson(
            np.log10(density), popt[0], popt[1], popt[2], popt[3], popt[4],
            popt[5], popt[6], popt[7], popt[8], popt[9], popt[10],
            popt[11], popt[12], popt[13], popt[14])

    def fit(self, p0, rhomin=1.e0):
        """Method to perform the fit using the Pearson formula"""
        # Perform the fit just on the core
        # TO-DO: this is no longer needed and should be removed
        condition = self.log10rho >= np.log10(rhomin)
        logrho = self.log10rho[condition]
        logP = self.log10P[condition]

        try:
            popt, _ = scipy.optimize.curve_fit(
                model_core, logrho, logP,
                p0=p0, xtol=self.xtol, method=self.fit_method)
        except Exception:
            log.warn('Curve fit failed. Reducing tolerance')
            popt, _ = scipy.optimize.curve_fit(
                model_core, logrho, logP,
                p0=p0, xtol=self.xtol*10., method=self.fit_method)

        return popt

    def bestfit(self, popt, density=None, cgs=False):
        """
        Return the pressure according to the best fit
        parameters, popt. Either use a density supplied
        as an input by the user, or use the densities
        from the table.

        """
        logrho = (
            self.log10rho if density is None
            else np.log10(density)
        )

        logP = model_core(
            logrho, popt[0], popt[1], popt[2], popt[3],
            popt[4], popt[5], popt[6], popt[7],
            popt[8], popt[9], popt[10], popt[11], popt[12],
            popt[13], popt[14],
            cgs=cgs
        )

        return np.power(10, logP, dtype=np.float64)

    def error(self, popt):
        """
        Calculate the relative error between the tabulated and fit
        values of the pressure.

        """
        return utils.calc_rel_error(self.pressure, self.bestfit(popt))
