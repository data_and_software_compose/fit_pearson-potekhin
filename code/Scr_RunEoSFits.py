#!/usr/bin/env python3
"""
Script that performs an analytical fit to one- or two-parameter
equations of state (i.e. pressure as a function of mass-energy
density, or pressure as a function of mass-energy density and
electronic charge fraction) from the CompOSE database. We use the
analytical respresentation described by Haensel & Potekhin 2004, A&A,
428, 191-197 and Pearson et al. 2018, MNRAS, 481, 2994-3026 to perform
a fit to the core region, while a polytropic model is added for the
crust.  We use the Generalised Piecewise Polytrope scheme described in
Boyle et al.  2020, Phys. Rev. D, 102, 083027 to ensure continuity in
pressure, sound speed and energy-density across the crust-core
boundary.

The analytical fits are then used to calculate thermodynamic
quantities needed for hydrodynamical simulations of oscillating
neutron stars.

"""
import argparse
import logging as log
import os

from fitter import (eos, plot, utils, compose)

# Define the different "types" of files created by the fitting
# scheme that correspond respectively to the eos.thermo file and
# the Lorene EoS file. These are used to plot the various thermodynamical
# quantities for diagnostic purposes.
FTYPES = ['thermo', 'lorene']
log.basicConfig(format='[%(levelname)s] %(module)s: %(asctime)s - %(message)s',
                level=log.INFO)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Make analytical fits for a two-parameter EoS',
        epilog=__doc__
    )

    parser.add_argument('config_file',
                        help='Path to configuration file that controls the '
                        'fitting procedure')
    args = parser.parse_args()

    fit_config = utils.read_config_file(args.config_file)
    # Check and delete any pre-existing output files before continuing.
    utils.check_existing_output(
        fit_config.get('eos', 'name')
    )

    table_path = fit_config.get('eos', 'table_path')
    tables = compose.CompOSE(
        fit_config.get('eos', 'url'),
        fit_config.get('eos', 'table_path'),
        fit_config.get('eos', 'compose_code')
    )

    # Create the tables needed for the fits as well as the
    # tables with which to compare the fitted data to, using
    # the canned eos.parameters and eos.quantities files for
    # the considered EoS.
    tables.download_data()

    for tabletype in ['core', 'ref']:
        tables.make_tables(
            os.path.join(table_path, tabletype, 'eos.quantities'),
            os.path.join(table_path, tabletype, 'eos.parameters'),
            f'eos.table.{tabletype}'
        )

    Yq_fit = eos.EoSFit(fit_config, 'eos.table.core')
    if fit_config.getboolean('fitting', 'run_fit'):
        log.info('Performing fits')
        Yq_fit.fit(eos.iT, write_diags=True)

    plot_configs = dict(fit_config.items('plots'))
    eos_name = fit_config.get('eos', 'name')
    if fit_config.getboolean('plots', 'make_plots'):
        log.info('Generating plots.')
        for ftype in FTYPES:
            plot.make_plots(
                plot_configs, eos_name, Yq_fit.eos.has_two_param,
                filetype=ftype
            )

    if not Yq_fit.eos.has_two_param:
        # Only perform housekeeping on one-parameter EoS tables
        # We will need the tables again for the two-parameter
        # EoS to generate the beta-equilibrated tables and
        # re-orthogonalise the the Lorene format table.
        tables.housekeep()
