[eos]
# URL to the EoS data on the CompOSE web-site
url = https://compose.obspm.fr/download//1D/NS/Skyrme/SLY4/
# Path to the tables generated by ComPOSE
table_path = ../inputs/eos-tables/${eos:name}/
# Path to the CompOSE software
compose_code = code-compose/
# Name of the EoS
name = SLy4

[fitting]
# Set to True to perform the fit plus addition of the polytropic crust
# Can be set to False if fitting has already been performed and plots
# are desired.
run_fit = True
# Tolerance used for the initial fit before the minimization procedure
tolerance = 1.e-4
# Fitting method (trf or lm)
method = trf
# Tolerance and method for the minimization procedure when adding the crust
mintol = 1.e-3
minmethod = trust-constr
# Value of the Baryon number density at the boundary between the crust
# and the intermediate layer
nBlim1 = 1.e-4
# Value of the Baryon number density at the boundary between the
# intermediate layer and the core
nBlim2 = 0.04
# Value of kappa for the polytropic crust (geometric units)
kappa = 0.140
# Value of gamma for the polytropic crust
gamma = 1.34

[nb_model_grid]
# Num. of grid points for the output model grid for the baryon number
# density
nsize = 200
# Start baryon number density (fm^-3)
start = 1.e-7
# End baryon number density (fm^-3)
end = 1.21e0
# Specify an input file containing the points to use in nB.
# Comment out to define a grid using the above parameters.
# grid_file=/home/pdavis/development/EoSforVirgo/eos-fits/sly4_fitted.d.copy

[plots]
# Set to True to create plots. Only works if comparing output
# where nB is on a regular grid and not specified via a file.
make_plots = True
# Directory where the plots should be written to
figdir = ../outputs/${eos:name}/figs
# Specify values of the charge fraction for which we want to
# plot various thermo variables as a function of mass-energy density,
# as created by the fitting scheme and the ComPOSE database.
yq_list = [0.0]
# Specify location of the eos.table that we want to compare the fits to.
# It should be on the same model grid for nb as the script output.
ref_table = ../inputs/eos-tables/${eos:name}/eos.table.ref
# Specify location of the Lorene-formatted file for two-parameter
# equation of state that we can use to compare the Pearson fits to.
# Comment out if no such file is available.
# ref_lorene = sly4_fitted.d
# Optional: specify the name of the Lorene file created from the
# fitting procedure. Will default to ../outputs/{eos}/eos_fit_{eos}.d
lorene_file = ../outputs/${eos:name}/eos_fit_${eos:name}.d
# Output error plots comparing fitted tables with the original
# CompOSE tables
plot_errors=True