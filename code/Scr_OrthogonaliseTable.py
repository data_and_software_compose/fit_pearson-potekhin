#!/usr/bin/env python3
"""
Script to re-write the Lorene table generated from the fitting procedure with
orthogonal values in the log-enthalpy (H) and the charge fraction (Ye). This
is needed since the mathematical formalism used by the hydrodynamical code is
based on a (H, Ye) grid.
The re-orthogonalisation proceeds as follows:

i) Define the new log-enthalpy grid, based on the first slice of Ye,
ii) Starting in the core, we solve for the new value of the baryon
number density for the new log-enthalpy at a given grid point,
iii) We then calculate the new values of pressure and energy density,
from which the other thermodynamic quantities can be calculated.

"""
import argparse
import logging as log
import os

import numpy as np
import scipy.integrate
import scipy.interpolate
import scipy.optimize

import fitter.eos
import fitter.pearson
import fitter.plot
import fitter.thermo
import fitter.utils
import fitter.consts
import fitter.compose

FIT_PARS_FMT = 'fit_parameters_{eos}_Yq_{yq:.4f}.dat'
CRUST_PARS_FMT = 'crust_parameters_{eos}.dat'
LORENE_FMT = 'eos_fit_{eos}.d'
LORENE_ORTHOG_FMT = 'eos_fit_{eos}_orthog.d'

log.basicConfig(format='[%(levelname)s] %(module)s: %(asctime)s - %(message)s',
                level=log.INFO)

fmt = [
    '%6i', '%24.16E', '%24.16E', '%24.16E', '%24.16E',
    '%24.16E', '%24.16E', '%24.16E', '%24.16E', '%24.16E'
]

columns = (
    '{0}{1:>30}{2:>28}{3:>18}{4:>30}{5:>25}{6:>25}{7:>33}'
    '{8:>25}'.format(
        'n_B [1/fm^3]', 'e [g/cm^3]', 'P [dyn/cm^3]', 'H [c^2]',
        'Y_e [dimensionless]', 'c_s^2 [c^2]', 'mu_e [MeV]',
        'd2p/dHdYe [dyn c^-2 cm^-2]', 'Prod rate [s^-1]')
)

header = (
    '# {eos_name} \n'
    '#\n'
    '# EoS created from fit of 2 parameter EoS. Parameters are n_B '
    'and Y_e\n'
    f'# {fitter.utils.git_branch_name()}'
    f'# {fitter.utils.git_latest_version()}'
    '{nbsize}    {Yesize} in n_B and Y_e\n'
    '#\n'
    f'#       {columns}\n'
    '#\n'
)


def orthogonalise(eos_table, crusts, df, shape):
    """
    Re-write the Lorene table so that the grid is orthogonal in
    log-enthalpy and the electron charge fraction.

    :arg: eos.EoS eos_table   : Instance of the eos.EoS class
    :arg: np.array crusts     : Array holding the crust parameters
                                attached to the core for each slice
                                along the electron charge fraction
    :arg: pandas.DataFrame df : Data frame holding the (non-orthogonalised)
                                Lorene data
    :arg: tuple shape         : Array size of the Lorene input data

    Returns:
        logHshift   : New grid in the log-enthalpy [c^2]
        nB_ortho    : New baryon number density [fm^-3]
        press_ortho : New pressure [dyn cm^-2]
        rho_ortho   : New energy_density [g cm^-3]

    """
    mn = eos_table.read_particle_info()['mn'].to_numpy()
    mn = mn[0]

    # Grab the polytropic parameters for the crust model
    kappa = crusts[:, 1]
    gamma = crusts[:, 2]
    nBlim1 = crusts[:, 3]
    nBlim2 = crusts[:, 4]

    # Now for the intermediate zone using the generalised
    # peice-wise polytrope
    kappaGPP = crusts[:, 5]
    gammaGPP = crusts[:, 6]
    LambdaGPP = crusts[:, 7]
    dGPP = crusts[:, 8]

    # Now for the core model, using the piece-wise polytrope
    # boundary conditions.
    Lambda = crusts[:, 9]
    d = crusts[:, 10]

    Yesize = shape[1]
    Nbsize = shape[0]

    logH = np.reshape(
        df[fitter.consts.ENTH_C2].to_numpy(), (Yesize, Nbsize))
    nB = np.reshape(
        df[fitter.consts.NB_FM3].to_numpy(), (Yesize, Nbsize))

    Yqgrid = eos_table.Ygrid

    nB_ortho = np.zeros((Yesize, Nbsize), dtype=np.float64)
    rho_ortho = np.zeros((Yesize, Nbsize), dtype=np.float64)
    press_ortho = np.zeros((Yesize, Nbsize), dtype=np.float64)

    # Define the fixed grid of values of the log enthalpy to solve for
    logHshift = logH[0, :] - logH[0, 0]

    # Lowest temperature entry in the table
    tmin = eos_table.Tgrid[0]

    for iY, Yq in enumerate(Yqgrid):
        log.info(f'Solving for Yq = {Yq}, iY = {iY}')

        # Grab the first grid point values within the core
        rhogrid = eos_table.search(
            fitter.consts.MASS_ENERGY_DENS_G_CM3, tmin, Yq)
        rho0 = rhogrid[0]

        # For this slice of the charge fraction, grab the initial
        # value of the log-enthalpy. Use this to scale the values
        # of the log-enthalpy for this slice.
        logH00 = logH[iY, 0]

        # Solve for the Baryon density for each of the new values of
        # the re-scaled log-enthalpy. Do this for i) the core,
        # ii) the intermediate zone and iii) the fixed crust region.
        core = nB[iY] >= nBlim2[iY]
        nBtest = nB[iY][core][-1]
        nBguess = nBtest

        fitparfile = FIT_PARS_FMT.format(eos=eos_name, yq=Yq)
        fitparpath = os.path.join(args.outpath, fitparfile)
        with open(fitparpath, 'r') as handle:
            fitpars = np.loadtxt(handle, skiprows=3, usecols=(2,))

        log.info('Solve for the core...')
        jn = len(logHshift) - 1
        while nBtest >= nBlim2[iY]:
            logHref = logHshift[jn]

            nBtest = fitter.utils.bisection(
                nBguess, calc_log_enthalpy, Lambda[iY],
                d[iY], eos_table, iY, tmin, fitter.pearson.model_core,
                fitpars, rho0, mn, logH00, logHref, tol=1.e-6
            )

            nBguess = nBtest
            rho = fitter.thermo._calc_rho(
                eos_table, iY, tmin, fitter.pearson.model_core, fitpars,
                rho0, nBtest)

            if nBtest >= nBlim2[iY]:
                rho_ortho[iY, jn] = fitter.thermo.energy_density(
                    rho, nBtest, Lambda[iY], d[iY]) / fitter.consts.C_CGS**2
                nB_ortho[iY, jn] = nBtest
                press_ortho[iY, jn] = pressure_pearson(
                    rho, fitpars, Lambda[iY])

            jn -= 1

        log.info('Solving for the intermediate regions.')
        jn += 1

        one_plus_dGPP = (1.e0 + dGPP[iY]) / fitter.consts.MEV_TO_ERG
        nBtest = nB[iY][jn]
        while nBtest >= nBlim1[iY]:

            logHref = logHshift[jn]
            nBtest = fitter.utils.bisection(
                nBlim2[iY], calc_log_enthalpy_poly, kappaGPP[iY],
                gammaGPP[iY], one_plus_dGPP, LambdaGPP[iY], mn,
                logH00, logHref)

            if nBtest >= nBlim1[iY]:
                nB_ortho[iY, jn] = nBtest
                press_ortho[iY, jn] = fitter.thermo.pressure_polytrope(
                    nBtest, kappaGPP[iY], gammaGPP[iY],
                    LambdaGPP[iY]/fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED) * \
                    fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED
                rho_ortho[iY, jn] = fitter.thermo.energy_density_polytrope(
                    nBtest, kappaGPP[iY], gammaGPP[iY], one_plus_dGPP,
                    LambdaGPP[iY]/fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED) * \
                    fitter.consts.MEV_TO_ERG / \
                    (fitter.consts.C_CGS**2 * fitter.consts.FM_TO_CM**3)

            jn -= 1

        log.info('Solving for the fixed-crust region.')

        jn += 1
        nBtest = nB[iY][jn]

        while jn >= 0:
            logHref = logHshift[jn]

            nBtest = fitter.utils.bisection(
                nBlim1[iY], calc_log_enthalpy_poly, kappa[iY],
                gamma[iY], mn, 0.e0, mn, logH00, logHref)

            if jn >= 0:
                nB_ortho[iY, jn] = nBtest
                press_ortho[iY, jn] = fitter.thermo.pressure_polytrope(
                    nBtest, kappa[iY], gamma[iY], 0.e0) * \
                    fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED
                rho_ortho[iY, jn] = fitter.thermo.energy_density_polytrope(
                    nBtest, kappa[iY], gamma[iY], mn, 0.e0) * \
                    fitter.consts.MEV_TO_ERG / \
                    (fitter.consts.C_CGS**2 * fitter.consts.FM_TO_CM**3)

            jn -= 1

    return logHshift, nB_ortho, press_ortho, rho_ortho


def pressure_pearson(rho, fitpars, Lambda):
    """
    Calculate the pressure [dyn/cm^2] using the Pearson-Potekhin
    fitting scheme.

    :arg: float rho     : Mass-energy density [g/cm^3]
    :arg: list fitpars  : Best-fit parameters [adim]
    :arg: float Lambda  : Boundary condition if generalised
                          peice-wise polytrope scheme is used [dyn/cm^2]

    """
    logrho = np.log10(rho)
    Ppearson = 10.e0**fitter.pearson.model_core(logrho, *fitpars, cgs=True)
    return Ppearson + Lambda


def calc_log_enthalpy(nB, Lambda, d, eos_table, iYq, tmin, fitfunc, fitpars,
                      rho0, mn, logH0=0.e0, logHref=0.e0):
    """
    Calculate the log-enthalpy [c^-2] based on the pressure and
    energy density calculated from the Pearson-Potekhin scheme.

    :arg: float nB                 : Baryon number density [fm^-3]
    :arg: float Lambda             : Pressure boundary condition for the
                                     GPP scheme [dyn/cm^2]
    :arg: float d                  : Energy boundary condition for the
                                     GPP scheme [erg]
    :arg: fitter.eos.EoS eos_table : Instance of the EoS class describing a
                                     CompOSE EoS table.
    :arg: int iYq                  : Index in charge fraction grid
    :arg: float tmin               : Lowest temperature entry for this
                                     EoS [MeV]
    :arg: func fitfunc             : Fit function used to fit P versus rho
    :arg: list fitpars             : Best-fit parameters
    :arg: float rho0               : First grid point in mass-energy
                                     density [g/cm^3]
    :arg: float mn                 : Neutron mass [MeV/c^2]
    :arg: float logH0              : Initial log enthalpy [c^-2]
    :arg: float logHref            : Reference log enthalpy [c^-2]

    """
    rho = fitter.thermo._calc_rho(
        eos_table, iYq, eos_table.Tgrid[0],
        fitfunc, fitpars, rho0, nB)

    press = pressure_pearson(rho, fitpars, Lambda) / \
        fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED
    energy = fitter.thermo.energy_density(rho, nB, Lambda, d) * \
        fitter.consts.FM_TO_CM**3 / fitter.consts.MEV_TO_ERG

    return np.log((press+energy) / (mn*nB)) - logH0 - logHref


def calc_log_enthalpy_poly(nB, kappa, gamma, mu0, Lambda, mn,
                           logH0=0.e0, logHref=0.e0):
    """
    Calculate the log-enthalpy [c^-2] based on the pressure and energy
    density based on the polytropic model.

    :arg: float nB         : Baryon density [1/fm^3]
    :arg: float kappa      : Proportionality constant in polytropic model
    :arg: float gamma      : Power law value in polytropic model
    :arg: float mu0        : Chemical potential at zero pressure (MeV)
    :arg: float mn         : Neutron mass (MeV)
    :arg: float logH0      : Offset to apply to the log enthalpy values
    :arg: float logHref    : Log enthalpy reference value (used when
                             solving for nB, for a given value logHref)

    """
    Lambda = Lambda / fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED

    press = fitter.thermo.pressure_polytrope(nB, kappa, gamma, Lambda)
    energy = fitter.thermo.energy_density_polytrope(
        nB, kappa, gamma, mu0, Lambda)

    return np.log((press+energy) / (mn*nB)) - logH0 - logHref


def square_of_sound_speed(Yesize, Nbsize, pressure, rho):
    """
    Calculate the square of the sound speed [c^-2].

    :arg: int Yesize        : Num. grid points in eletron charge fraction
    :arg: int Nbsize        : Num. grid points in baryon number density
    :arg: np.array pressure : Pressure [dyn cm^-2]
    :arg: np.array rho      : Mass-energy density [g/cm^3]

    """
    cs2 = np.zeros((Yesize, Nbsize), dtype=np.float64)
    energy = rho * fitter.consts.C_CGS * fitter.consts.C_CGS * \
        fitter.consts.FM_TO_CM**3 / fitter.consts.MEV_TO_ERG

    for iY in range(cs2.shape[0]):
        cs2[iY] = np.gradient(
            pressure[iY] / fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED,
            energy[iY], edge_order=2)

    return cs2


def calc_d2p_dlogHdY(Yesize, Nbsize, pressure, rho, nB, Yegrid):
    """
    Second derivative of pressure w.r.t log-enthalpy and charge
    fraction.

    [dyn c^-2 cm^-2].

    :arg: int Yesize        : Num. grid points in eletron charge fraction
    :arg: int Nbsize        : Num. grid points in baryon number density
    :arg: np.array pressure : Pressure [dyn cm^-2]
    :arg: np.array rho      : Mass-energy density [g/cm^3]
    :arg: np.array nB       : Baryon number density [fm^-3]
    :arg: np.array Yegrid   : Grid in electronic charge fraction

    """
    energy = rho * fitter.consts.C_CGS * fitter.consts.C_CGS
    dPdlogH = (pressure + energy)

    return np.gradient(dPdlogH, Yegrid, axis=0, edge_order=2)


def calc_chemical_potential(Yesize, Nbsize, rho, nB, Yegrid, mn):
    """
    Calculate the lepton chemical potential. The derivative should be
    calculated for fixed baryon number density. However, since this is
    no longer the case due to the re-orthogonalisation we i) regrid
    the scaled energy density onto a new grid for nB, ii) calculate
    the chemical potential on this grid, iii) use interpolation to
    calculate the chemical potential on the values of nB calculated
    after the re-orthogonalisation step.

    :arg: int Yesize      : Num. grid points in electronic charge fraction
    :arg: int Nbsize      : Num. grid points in baryon num. density
    :arg: np.array rho    : Mass-energy density [g/cm^3]
    :arg: np.array nB     : Baryon num. density [fm^-3]
    :arg: np.array Yegrid : Grid for electronic charge fraction [adim]
    :arg: float Mn        : Neutron mass [MeV/c^2]

    """
    energy = rho * fitter.consts.C_CGS * fitter.consts.C_CGS * \
        fitter.consts.FM_TO_CM**3 / fitter.consts.MEV_TO_ERG
    escaled = (energy / (nB * mn)) - 1.e0

    # Regrid the scaled energy onto a regular nB grid
    nBnew = np.linspace(
        np.log10(np.min(nB)), np.log10(np.max(nB)), 200)
    nBnew = 10.e0**nBnew
    escaled_regridded = []

    for i in range(Yesize):
        fint = scipy.interpolate.interp1d(
            nB[i], escaled[i], 'cubic', fill_value='extrapolate')

        escaled_regridded.append(fint(nBnew))

    escaled_regridded = np.array(escaled_regridded)
    mul = mn * np.gradient(escaled_regridded, Yegrid, axis=0, edge_order=2)

    mul_interp = []
    for i in range(Yesize):
        muint = scipy.interpolate.interp1d(
            nBnew, mul[i], 'cubic', fill_value='extrapolate')

        mul_interp.append(muint(nB[i]))

    return np.array(mul_interp)


parser = argparse.ArgumentParser()
parser.add_argument('outpath',
                    help='Path to output from fitting procedure.')
parser.add_argument('config_file',
                    help='Path to the configuration file that controls '
                    'the fitting procedure.')
parser.add_argument('--plot-only', '-p', action='store_true',
                    help='Set to True to only perform the plots of the '
                    'orthogonalised table.')

args = parser.parse_args()

fit_config = fitter.utils.read_config_file(args.config_file)

table_dir = fit_config.get('eos', 'table_path')
table_path = os.path.join(table_dir, 'eos.table.core')

thermo_table = fit_config.get('eos', 'table_path')
eos_name = fit_config.get('eos', 'name')

tables = fitter.compose.CompOSE(
    fit_config.get('eos', 'url'),
    table_dir,
    fit_config.get('eos', 'compose_code')
)

tables.download_data()
for tabletype in ['core', 'ref']:
    tables.make_tables(
            os.path.join(table_dir, tabletype, 'eos.quantities'),
            os.path.join(table_dir, tabletype, 'eos.parameters'),
            f'eos.table.{tabletype}'
    )

eos_table = fitter.eos.EoS(table_path, thermo_table)

# Read in the polytropic crust model information for each
# slice of the charge fraction.
crustinfo = CRUST_PARS_FMT.format(eos=eos_name)
crustpath = os.path.join(args.outpath, crustinfo)
crusts = np.loadtxt(crustpath, skiprows=1)

lorenefile = LORENE_FMT.format(eos=eos_name)
lorenepath = os.path.join(args.outpath, lorenefile)
df = fitter.utils.read_lorene_file(lorenepath, eos_2par=True)
shape = fitter.utils.lorene_grid_size(lorenepath)
fname = LORENE_ORTHOG_FMT.format(eos=eos_name)

if not args.plot_only:
    Yesize = shape[1]
    Nbsize = shape[0]
    Yqgrid = eos_table.Ygrid

    Ye = np.reshape(
        df[fitter.consts.YQ].to_numpy(),
        (Yesize, Nbsize))

    mn = eos_table.read_particle_info()['mn'].to_numpy()
    mn = mn[0]

    logHshift, nB_ortho, press_ortho, rho_ortho = orthogonalise(
        eos_table, crusts, df, shape)

    cs2 = square_of_sound_speed(Yesize, Nbsize, press_ortho, rho_ortho)
    d2p_dlogHdY = calc_d2p_dlogHdY(
        Yesize, Nbsize, press_ortho, rho_ortho, nB_ortho, Yqgrid)
    logHnew = np.broadcast_to(logHshift, (Yesize, Nbsize)).flatten()
    mul = calc_chemical_potential(
        Yesize, Nbsize, rho_ortho, nB_ortho, Yqgrid, mn)
    nb_indices = np.broadcast_to(
        np.array(range(Nbsize)), (Yesize, Nbsize)).flatten()
    prod_rate = np.zeros((Yesize*Nbsize, ))

    outdata = [
        nb_indices,
        nB_ortho.flatten(), rho_ortho.flatten(), press_ortho.flatten(),
        logHnew, Ye.flatten(), cs2.flatten(), mul.flatten(),
        d2p_dlogHdY.flatten(), prod_rate
    ]

    fpath = os.path.join(args.outpath, fname)
    with open(fpath, 'w') as fh:
        fh.write(header.format(
            eos_name=eos_name, nbsize=Nbsize, Yesize=Yesize))
        np.savetxt(fh, np.column_stack(outdata), fmt)

log.info('Generating plots.')
plot_configs = dict(fit_config.items('plots'))

# Over-ride some of the plotting settings so that we use the
# orthogonalised table, update the output directory for the plots.
# Since the grid in nB is now different w.r.t the ComPOSE table
# turn off the plotting of the errors.
plot_configs['figdir'] = os.path.join(plot_configs['figdir'], 'orthog')
plot_configs['lorene_file'] = plot_configs['lorene_file'].replace(
    f'eos_fit_{eos_name}.d', fname)
plot_configs['plot_errors'] = False

fitter.plot.make_plots(
    plot_configs, eos_name, True, filetype='lorene'
)

tables.housekeep()
