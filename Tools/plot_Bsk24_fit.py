#!/usr/bin/env python3
"""
Plot the Pearson-Potekhin analytical representation for BSk24. The
fit expression used as well as the fit coefficients can be found in
Pearson et al. 2018, MNRAS, 481, 2994-3026. The script also outputs
mass-energy density (g/cm^3) and pressure (dyn/cm^2) for the fit, the
CompOSE tabulated data and for each of the terms in the fitting
expression.

"""
import os
import sys

import numpy as np
import matplotlib.pyplot as plt

sys.path.append('../code')

import fitter.consts


def writedata(fname, rho, press):
    """
    Write mass-energy density, rho, and the pressure, press,
    into a file named fname.

    """
    with open(fname, 'w') as handle:
        np.savetxt(handle, np.column_stack([rho, press]))


def f0(a, b, c):
    """Define a Fermi-like function used in the fit expression."""
    return 1.e0 / (np.exp(a*(b - c)) + 1.e0)


def term1(xi):
    """
    First term of the fit, along with the fit coefficients.
    Corresponds to the outer crust of the neutron star.

    """
    p1 = 6.795
    p2 = 5.552
    p3 = 0.00435
    p4 = 0.13963
    p5 = 3.636
    p6 = 11.943

    numer = (p1 + p2*xi + p3*xi**3)
    denom = (1.e0 + p4*xi)

    return (numer/denom) * f0(p5, xi, p6)


def term2(xi):
    """
    Second term of the fit, along with the fit coefficients.
    Corresponds to the inner crust/ core of the neutron star.

    """
    p6 = 11.943
    p7 = 13.848
    p8 = 1.3031
    p9 = 3.644

    return (p7 + p8*xi) * f0(p9, p6, xi)


def term3(xi):
    """
    Third term of the fit, along with the fit coefficients.
    Corresponds to the inner crust/ core of the neutron star.

    """
    p10 = -30.840
    p11 = 2.2322
    p12 = 4.65
    p13 = 14.290

    return (p10 + p11*xi) * f0(p12, p13, xi)


def term4(xi):
    """
    Fourth term of the fit, along with the fit coefficients.
    Corresponds to the inner crust/ core of the neutron star.

    """
    p14 = 30.08
    p15 = -2.080
    p16 = 1.10
    p17 = 14.71

    return (p14 + p15*xi) * f0(p16, p17, xi)


def term5(xi):
    """
    Fifth term of the fit, along with the fit coefficients.
    Corresponds to the neutron drip.

    """
    p18 = 0.099
    p19 = 11.66
    p20 = 5.00

    return p18 / (1.e0 + (p20*(xi - p19))**2)


def term6(xi):
    """
    Sixth term of the fit, along with the fit coefficients.
    Corresponds to the core-crust boundary.

    """
    p21 = -0.095
    p22 = 14.15
    p23 = 9.1

    return p21 / (1.e0 + (p23*(xi - p22))**2)


if __name__ == '__main__':

    # Get original tabulated data
    eos_data = 'data/'
    eos_table = 'eos.table'
    eos_path = os.path.join(eos_data, eos_table)

    with open(eos_path, 'r') as handle:
        Ptab, epsilon_tab = np.loadtxt(
            handle, usecols=(3, 8), unpack=True)

    Ptab = Ptab * fitter.consts.MEV_FMCUBED_TO_ERG_CMCUBED
    epsilon_tab = epsilon_tab * fitter.consts.eps_convert

    # Set K to zero to calculate the pressure in dyn/cm^2
    K = 0
    xi = np.linspace(6, 15.8, 200)
    epsilon_grid = 10.**xi
    zeta = K + term1(xi) + term2(xi) + term3(xi) + term4(xi) + \
        term5(xi) + term6(xi)

    plt.figure(figsize=(12, 5))

    # Left panel: Pressure versus mass-energy density over full
    # range
    plt.subplot(1, 2, 1)
    plt.plot(epsilon_tab, Ptab, '-k', linewidth=3)
    plt.plot(epsilon_grid, 10.**zeta, '-r')
    plt.plot(epsilon_grid, 10.**(term1(xi)), '-.b')
    plt.plot(epsilon_grid, 10.**(term2(xi)), '-.r')
    plt.plot(epsilon_grid, 10.**(term3(xi)), ':r')
    plt.plot(epsilon_grid, 10.**(term4(xi)), '--r')
    plt.plot(epsilon_grid, 10.**(term5(xi)), '-.g')
    plt.plot(epsilon_grid, 10.**(term6(xi)), ':g')

    writedata('Bsk24_fit.dat', epsilon_grid, 10.**zeta)
    writedata('Bsk24_compOSE.dat', epsilon_tab, Ptab)
    writedata('Bsk24_fit_term1.dat', epsilon_grid, 10.**(term1(xi)))
    writedata('Bsk24_fit_term2.dat', epsilon_grid, 10.**(term2(xi)))
    writedata('Bsk24_fit_term3.dat', epsilon_grid, 10.**(term3(xi)))
    writedata('Bsk24_fit_term4.dat', epsilon_grid, 10.**(term4(xi)))
    writedata('Bsk24_fit_term5.dat', epsilon_grid, 10.**(term5(xi)))
    writedata('Bsk24_fit_term6.dat', epsilon_grid, 10.**(term6(xi)))

    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'e [g cm$^{-2}$]')
    plt.ylabel(r'P [dyn cm$^{-2}$]')

    # Right panel: Zoom-in
    plt.subplot(1, 2, 2)
    plt.plot(epsilon_grid, 10.**zeta, '-r')
    plt.plot(epsilon_grid, 10.**(term1(xi)), '-.b')
    plt.plot(epsilon_grid, 10.**(term2(xi)), '-.r')
    plt.plot(epsilon_grid, 10.**(term3(xi)), ':r')
    plt.plot(epsilon_grid, 10.**(term4(xi)), '--r')
    plt.plot(epsilon_grid, 10.**(term5(xi)), '-.g')
    plt.plot(epsilon_grid, 10.**(term6(xi)), ':g')

    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.1, 10)

    plt.xlabel(r'e [g cm$^{-3}$]')
    plt.ylabel(r'P [dyn cm$^{-2}$]')

    plt.savefig('Bsk24_fit.png')
