## Provenance for Bsk24 equation of state

* Data downloaded from `https://compose.obspm.fr/eos/253` and `eos.table` created using the `CompOSE` software via `eos.parameters` and `eos.quantities` files located in `data/`.